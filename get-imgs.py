#!/usr/bin/python

import re
import os
import sys
import glob
import hashlib
import binascii
import requests

geturl="http://fotoalbum.seniorennet.be/incl/"
fotos="public/fotoalbum.seniorennet.be/fons/fotos/"

def imglinktomd5(imglink):

  imglink = imglink.strip()	
  m = hashlib.new('md5')
  m.update(binascii.hexlify(imglink)) 
  newimg = m.hexdigest()  
  return newimg

if len(sys.argv)>1: 
	print(sys.argv)
	htmlfile=sys.argv[1]


for imglink in open(htmlfile):      
  imglink = imglink.strip()	  
  url = "http://fotoalbum.seniorennet.be/incl/"+imglink+"&typeid=8"
  z = re.match("getimage2.php\?imageid=([0-9]+)&albumid=[0-9]+", imglink)
  if z:
    img=z.group(1)
    if not os.path.exists(fotos+"img"+img+".jpg"):
      print(img)
      r = requests.get(url)
      with open(fotos+"img"+img+".jpg", "wb") as f:
        f.write(r.content)

  
  
