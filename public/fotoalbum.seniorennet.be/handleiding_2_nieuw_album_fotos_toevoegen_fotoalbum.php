</html>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-15" />
    <meta http-equiv="content-language" content="nl" />
            <meta name="Author" content="Seniorennet" />
    <meta http-equiv="Expires" content="Fri, 22 Jan 2021 17:15:18 CET" />
    <meta http-equiv="Pragma" content="no-cache" />
    <meta http-equiv="Cache-Control" content="no-cache" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    
    <title>SeniorenNet - Voor mensen met levenservaring en levenswijsheid</title>
    
          <!-- CSS NIEUWE LAYOUT -->


    <link rel="stylesheet" href="//www.seniorennet.be/css/print.css" type="text/css" media="print" />
    <link type="text/css" rel="stylesheet" href="//www.seniorennet.be/css/style.css?v20170717" />
    <link type="text/css" rel="stylesheet" href="//www.seniorennet.be/css/interactief.css?v=2" />
    <link type="text/css" rel="stylesheet" href="//www.seniorennet.be/css/fotoalbum.css" />
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
<link type="text/css" rel="stylesheet" href="https://www.seniorennet.be/css/sennet.css?v7" />


    <script src="https://www.google.com/jsapi?key=ABQIAAAA3sU-PhfOZtZRP_7JAqF6QRTBfbnnDyDqgyIAPZ5-G9HwiDjFoBTLf_CQBpr7R0r-7YUMhhY23aF0xQ" type="text/javascript"></script>

    <!--[if lt IE 9]>
        <link type="text/css" rel="stylesheet" href="https://www.seniorennet.be/cache_css/ie8.css?v1" />
            <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
            <script type="text/javascript" src="https://www.seniorennet.be/cache_js/ie8-head.js"></script>
    <![endif]-->
    <!--[if gt IE 8]><!-->
          <script src="//ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
        <!--<![endif]-->
    <script type="text/javascript" src="https://www.seniorennet.be/js/sennet.js"></script>

    
    
    

     
    

<script async='async' src='https://www.googletagservices.com/tag/js/gpt.js'></script>
<script>
    var googletag = googletag || {};
    googletag.cmd = googletag.cmd || [];
</script>

<script type='text/javascript'>
  googletag.cmd.push(function() {
    googletag.defineSlot('/8188103/SENNET_ROS_CONTENT1', [[300, 600], [300, 250]], 'div-gpt-ad-1462356799442-1').addService(googletag.pubads()).setCollapseEmptyDiv(true);
    googletag.defineSlot('/8188103/SENNET_ROS_CONTENT2', [[300, 600], [300, 100], [300, 250]], 'div-gpt-ad-1462356799442-2').addService(googletag.pubads()).setCollapseEmptyDiv(true);
    googletag.defineSlot('/8188103/SENNET_ROS_TOP', [[995, 125], [995, 123], [728, 90], [970, 90], [970, 250]], 'div-gpt-ad-1462356799442-0').addService(googletag.pubads()).setCollapseEmptyDiv(true);
    googletag.defineSlot('/8188103/SN_BE_Fotoalbum_Logo', '[150x125]', 'div-gpt-ad-1462356799442-3').addService(googletag.pubads()).setCollapseEmptyDiv(true);
    googletag.defineSlot('/8188103/SN_BE_Fotoalbum_Rectangle_Partner', '[150x125]', 'div-gpt-ad-1462356799442-4').addService(googletag.pubads()).setCollapseEmptyDiv(true);
    googletag.pubads().setTargeting('rubriek', ['old']);
    googletag.pubads().enableSingleRequest();
    googletag.enableServices();
  });

  function GA_googleFillSlot()
  {
    console.log('oldGAMTag');
  }
</script>
    
        
    </head>
<body  >
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-1117128-1', 'auto');
  ga('require', 'linkid', 'linkid.js');
  ga('require', 'displayfeatures');
  ga('send', 'pageview');

  var trackOutboundLink = function(url) {
    ga('send', 'event', 'outbound', 'click', url, {'hitCallback':
      function () {document.location = url;}
    });
  }

</script>

<script defer src="https://use.fontawesome.com/releases/v5.0.6/js/all.js"></script>

<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/nl_NL/all.js#xfbml=1";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>


   <!-- NIEUWE HEADER -->
<div id="page" class="row main-container">
  <div class="columns xlarge-12 small-12">
    <div id="header" class="row">
      <div class="user-menu row show-for-large" id="user-menubar">

        <div class="columns medium-3  menu-text main-logo">
            <a href="https://www.seniorennet.be/"><img src="https://www.seniorennet.be/img/layout/logo-seniorennet.svg?1516978662" alt="seniorennet" title="seniorennet" class="sn-logo hide-for-small-only" onerror="this.src=&#039;http:\/\/www.seniorennet.be/img/layout/logo-seniorennet.png&#039;"/></a>    
        </div>

        <div class="columns medium-5 xlarge-6">
            <form id="searchbox_003717546627450800428:obdjujbywam" action="https://www.seniorennet.be/zoeken/">
                <input value="003717546627450800428:obdjujbywam" name="cx" type="hidden"/>
                <input value="FORID:11" name="cof" type="hidden"/>
                <div class="search-button input-group">
                    <input id="q" name="q" type="search" class="input-group-field" placeholder="Zoeken">
                    <div class="input-group-button">
                        <button type="submit" name="sa" class="button"><i class="fas fa-search" data-fa-transform="left-6"></i></button>
                        <input type="submit" value="" />
                    </div>
                </div>
            </form>
        </div>
        <div class="columns medium-3 text-right" style="margin-left: -10px;">
            <ul>
                          <li><a class="" href="https://www.seniorennet.be/login"> <i class="hide-for-large fas fa-user"></i>Aanmelden</a></li>
              <li><a class="red" href="https://www.seniorennet.be/register">Registreren</a></li>
                          </ul>
        </div>
      </div>




      <div class="top-bar is-at-top" id="main-menubar">

        <div class="top-bar-left">
            <ul class="horizontal dropdown menu" data-dropdown-menu>
                <li id="sub-logo">
                    <a href="/"><img src="https://www.seniorennet.be/img/layout/logo-seniorennet.svg?1516978662" alt="seniorennet" title="seniorennet" onerror="this.src=&#039;https:\/\/www.seniorennet.be/img/layout/logo-seniorennet.png&#039;"/></a>
                </li>
                <li class="has-submenu">
                    <a href="https://www.seniorennet.be/nieuws">Nieuws</a>
                </li>
                <li class="has-submenu">
                    <a href="https://www.seniorennet.be/rubriek/gezondheid">Gezondheid</a>

                </li>
                <li class="has-submenu">
                    <a href="https://www.seniorennet.be/rubriek/wonen">Wonen</a>

                </li>
                <li class="has-submenu">
                    <a href="https://www.seniorennet.be/rubriek/geld-en-werk">Geld &amp; Recht</a>

                </li>
                <li class="has-submenu">
                    <a href="https://www.seniorennet.be/rubriek/toerisme">Toerisme</a>

                </li>
                <li class="has-submenu">
                    <a href="#">Vrije Tijd</a>

                    <ul class="submenu menu is-dropdown-submenu double" data-submenu>
                        <li><a href="https://www.seniorennet.be/Pages/grappig_schattig/">Grappig of Schattig</a></li>
                        <li><a href="/page/info/agenda">Activiteiten</a></li>
                        <li><a href="http://administratie.seniorennet.be/Nieuwsbrief/index_sennet_plezier.php">Dagelijks Grapje</a></li>
                        <li><a href="https://www.seniorennet.be/Pages/Vrije_tijd/spelletjes.php">Spelletjes</a></li>
                        <li><a href="https://www.seniorennet.be/Pages/Vrije_tijd/vrije_tijd.php">Vrije Tijd</a></li>
                        <li><a href="https://www.seniorennet.be/Pages/Vrije_tijd/Quiz/quiz.php">Quiz</a></li>
                        <li><a href="https://www.seniorennet.be/Pages/Visitekaartjes/visitekaartjes.php">Visitekaartjes</a></li>
                        <li><a href="https://www.seniorennet.be/ecards/">E-cards</a></li>
                        <li><a href="http://fotografie.seniorennet.be/">Fotografie</a></li>
                        <li><a href="https://www.seniorennet.be/rubriek/culinair/">Culinair</a></li>
                        <li><a href="https://www.seniorennet.be/rubriek/tuinkriebels/">Tuinkriebels</a></li>
                    </ul>
                </li>
                <li class="has-submenu">
                    <a href="#">Community</a>

                    <ul class="submenu menu is-dropdown-submenu " data-submenu>
                        <li><a href="https://www.seniorennet.be/pages/redactie">Nieuwsbrief</a></li>
                        <li><a href="https://www.seniorennet.be/forum/">Forum</a></li>
                        <li><a href="http://blog.seniorennet.be/">Blogs</a></li>
                        <li><a href="https://www.seniorennet.be/Pages/Vrije_tijd/chatbox.php">Chatbox</a></li>
                        <li><a href="https://www.seniorennet.be/Pages/Mailgroepen/mailgroepen.php">Mailgroepen</a></li>
                        <li><a href="http://fotoalbum.seniorennet.be/">Foto-album</a></li>
                        <li><a href="https://www.seniorennet.be/Pages/Zoeken/zoeken.php">Zoekertjes</a></li>
                        <li><a href="http://webmail.seniorennet.be/index.php">Webmail</a></li>
                    </ul>
                </li>
                <li class="has-submenu">
                    <a href="#">Nuttig</a>

                    <ul class="submenu menu is-dropdown-submenu " data-submenu>
                        <li><a href="https://www.seniorennet.be/pages/nuttige-links">Nuttige Info</a></li>
                        <li><a href="https://www.seniorennet.be/Pages/tips/tips.php">Tips</a></li>
                        <li><a href="https://www.seniorennet.be/Pages/Weer/weer.php">Weer</a></li>
                        <li><a href="https://www.seniorennet.be/Pages/Weer/buienradar.php">Buienradar</a></li>
                        <li><a href="https://www.seniorennet.be/Pages/Auto/verkeersinfo.php">Verkeersinfo</a></li>
                        <li><a href="https://www.seniorennet.be/rubriek/thuis-op-internet/">Thuis op Internet</a></li>
                        <li><a href="https://www.seniorennet.be/Pages/Thuis_op_internet/computerhulp.php">Computerhulp</a></li>
                        <li><a href="https://www.seniorennet.be/Pages/Vrije_tijd/omrekenen.php">Omrekenen</a></li>
                    </ul>
                </li>




            </ul>
        </div>

        <div class="top-bar-right">
            <ul class="dropdown menu" data-dropdown-menu id="menu-topicon" >
                <li>
                    <a href="/zoeken"><i class="fas fa-search fa-lg" data-fa-transform="right-1"></i></a>
                </li>
                <li>
                    <a href="#"><i class="fas fa-user-circle fa-lg" data-fa-transform="right-1"></i></a>
                    <ul class="submenu menu is-dropdown-submenu" data-submenu>
                        <li><a class="" href="https://www.seniorennet.be/login"> Aanmelden</a></li>
                        <li><a href="https://www.seniorennet.be/login">Registreren</a></li>
                    </ul>
                </li>
            </ul>
        </div>

    </div>



</div><!-- #header -->

    <div class="columns content">
      <!-- CONTENT -->




 <!-- EINDE NIEUWE HEADER -->





  </div>
  <div id="wrapper">
    <div id="wrapper-inner">

      <div id="advertisement" class="top">
          <div id="advertisement-inner">
            
            <div id='div-gpt-ad-1462356799442-0'>
            <script type='text/javascript'>
            googletag.cmd.push(function() { googletag.display('div-gpt-ad-1462356799442-0'); });
            </script>
            </div>  
            <div class="clear"></div>
          </div>
        </div>
      <div id="wrapper-inner-content">
      <div id="main" class="row">
    <div class="" id="content">

    <style type="text/css">
<!--
p.MsoNormal {
margin-top:0cm;
margin-right:0cm;
margin-bottom:10.0pt;
margin-left:0cm;
}
-->
</style>
<div class="body"> 
  <table width="100%" border="0" cellspacing="5" cellpadding="0">
    <tr> 
      <td valign="top"> 
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr> 
            <td bgcolor="#BABA74" height="20"><font color="#003333"> Waar vandaan: <a href="index.php">Foto albums</a> &gt; <a href="help.php">Help</a> &gt; Stap 2: nu starten met foto-album</font></td>
          </tr>
        </table>
        <h1 align="left"><strong>Stap 2: nu starten</strong>: een nieuw album maken en foto's toevoegen</h1>
        <p class="MsoNormal">Als je eenmalig geregistreerd bent zoals eerder uitgelegd,  dan kan je nu echt aan de slag. We gaan nu een album aanmaken en deze vullen  met een aantal foto&rsquo;s. </p>
        <p class="MsoNormal">De eerste stap die je altijd zal moeten doen is surfen naar <a href="http://fotoalbum.seniorennet.be">http://fotoalbum.seniorennet.be</a> en  vervolgens moet je inloggen.&nbsp; Dit  inloggen is het opgeven van je gebruikersnaam en wachtwoord dat we eerder zelf  hebben gekozen.&nbsp; Geef deze gegevens  in.&nbsp; Indien je niet elke keer deze wil  ingeven, kan je het vierkantje aanklikken &ldquo;Bewaar gegevens&rdquo;.&nbsp; Klik op de knop &ldquo;verzenden&rdquo;met je  linkermuisknop.</p>
        <p align="center" class="MsoNormal"><img border = "1" src="images/help/fotoalbum_7.jpg" width="450" height="240" /></p>
        <p class="MsoNormal">We willen een nieuw fotoalbum aanmaken, je klikt in het menu  dat verschijnt op &ldquo;Nieuw fotoalbum aanmaken&rdquo;, en dat doe je met de  linkermuisknop.</p>
        <p align="center" class="MsoNormal"><img border = "1" src="images/help/fotoalbum_8.jpg" width="450" height="81" /></p>
        <p class="MsoNormal">Als eerste geven we de titel van ons fotoalbum in.&nbsp; Dit kan eender wat zijn dat je zelf kiest. &nbsp;Ik kies voor dit voorbeeld de naam  &ldquo;natuurwandeling&rdquo;.&nbsp; </p>
        <p class="MsoNormal">Het tweede dat je kan kiezen is het unieke adres waar het  fotoalbum zal op te vinden zijn.&nbsp;  Standaard is dat gelijk aan de titel, maar je kan dit ook anders  doen.&nbsp; Het adres is steeds <a href="http://fotoalbum.seniorennet.be/%5buw_gekozen_adres%5d/%5bgekozen_fotoalbum">http://fotoalbum.seniorennet.be/[uw_gekozen_adres]/[gekozen_fotoalbum</a>],  dus je eigen gekozen adres dat je koos bij de registratie, gevolgd door een  zelf gekozen naam voor je fotoalbum.&nbsp; </p>
        <p class="MsoNormal">Vervolgens kies je een categorie waar het album toe zal  behoren.&nbsp; En vervolgens kies je een  subcategorie.&nbsp; Omdat mijn fotoalbum gaat  over een natuurwandeling, kies ik voor &ldquo;natuur en landschappen&rdquo;.</p>
        <p class="MsoNormal">Je kan nu nog kiezen voor wie het fotoalbum zichtbaar zal  zijn.&nbsp; Standaard is dat gewoon voor  iedereen.&nbsp; Maar wil je het priv&eacute; houden  en enkel voor vrienden of familie zichtbaar maken, dan is dat ook mogelijk.&nbsp; <br />
        Ben je klaar, dan druk je op de knop &ldquo;Verzenden&rdquo;.</p>
        <p align="center" class="MsoNormal"><img border = "1" src="images/help/fotoalbum_9.jpg" width="450" height="278" /></p>
        <p class="MsoNormal">Je album is aangemaakt!&nbsp;  Zo eenvoudig is dat!&nbsp; Nu moet je  natuurlijk nog wel foto&rsquo;s toevoegen aan het album.&nbsp; Je krijgt de pagina hiervoor al onmiddellijk  te zien.</p>
        <p align="center" class="MsoNormal"><img border = "1" src="images/help/fotoalbum_10.jpg" width="450" height="278" /></p>
        <p class="MsoNormal">Je hebt diverse mogelijkheden om foto&rsquo;s toe te voegen.&nbsp; Je kan de bestanden een voor een selecteren  van je computer, je kan foto per foto toevoegen, je kan ze via e-mail toevoegen  of als je ze al ergens op een internetadres hebt staan ze zo toevoegen.&nbsp; Maar, het handigste van allemaal is de eerste  mogelijkheid die je ziet staan: &ldquo;snel en eenvoudig&rdquo;.&nbsp; Hiermee kan je zoveel foto&rsquo;s toevoegen en dit  heel gebruiksvriendelijk.&nbsp; Klik op de  knop &ldquo;Voeg foto&rsquo;s toe van uw computer met handige plugin&rdquo;.&nbsp; </p>
        <p class="MsoNormal">Er zal een nieuw scherm worden geopend.&nbsp; Dit nieuwe scherm gaat zorgen voor het  eenvoudig toevoegen van je foto&rsquo;s.&nbsp; De  allereerste keer ga je wel iets speciaals moeten doen.&nbsp; Het zal eerst een aantal seconden een blanco  scherm zijn.&nbsp; Daarna zal er een melding  komen.&nbsp; Afhankelijk van je browser en je  besturingssysteem (XP of Vista) ziet dat er anders uit. </p>
        <p class="MsoNormal">Indien bovenaan een balk verschijnt zoals op volgende foto,  klik je er met je linkermuisknop op en klik je op &ldquo;ActiveX-besturingselement  installeren&rdquo;.&nbsp; </p>
        <p align="center" class="MsoNormal"><img border = "1" src="images/help/uploader3.jpg" width="450" height="281" /></p>
        <p class="MsoNormal">Indien er meldingen komen, klik je steeds op &ldquo;Installeren&rdquo;,  &ldquo;Install&rdquo;, &ldquo;OK&rdquo; of &ldquo;Doorgaan&rdquo;.&nbsp; </p>
        <p align="center" class="MsoNormal"><img border = "1" src="images/help/uploader4.jpg" width="450" height="223" /></p>
        <p class="MsoNormal">Hoogstens enkele minuten duurt deze installatie.&nbsp; Het is gelukkig maar eenmalig.&nbsp; Alle volgende keren in de toekomst hoef je  dit niet meer te doen.&nbsp; Het zal dan  allemaal onmiddellijk verschijnen. </p>
        <p class="MsoNormal">Je krijgt nu het scherm te zien om je foto&rsquo;s te selecteren.</p>
        <p align="center" class="MsoNormal"><img border = "1" src="images/help/fotoalbum11.jpg" width="450" height="317" /></p>
        <p class="MsoNormal">Links zie je de mappen van je computer.&nbsp; Klik de juiste map aan waar de foto&rsquo;s staan  die je wil toevoegen aan je album.&nbsp; Heb  je op die map geklikt, dan zal rechtsboven in het vak al de foto&rsquo;s uit die map  verschijnen.</p>
        <p class="MsoNormal">Nu kan je kiezen welke foto&rsquo;s je effectief wil gaan  toevoegen.&nbsp; Wil je een of enkele foto&rsquo;s  uit die map, dan kan je ze aanklikken en slepen naar het onderste witte  vak.&nbsp; Ofwel klik je ze aan met je  linkermuisknop en klik je op de knop &ldquo;Toevoegen&rdquo;.&nbsp; <br />
        Wil je alle foto&rsquo;s uit die map toevoegen, dan kan je gewoon klikken op de knop  &ldquo;Alles toevoegen&rdquo; die je in het midden van het scherm kan vinden. </p>
        <p class="MsoNormal">Indien je nog foto&rsquo;s wil toevoegen, kan je nog andere mappen  openen en verder foto&rsquo;s toevoegen naar het vak rechtsonder.&nbsp;&nbsp;&nbsp; Heb je een foto per ongeluk toegevoegd die  je toch niet wil, dan klik je ze aan en druk je op de knop &ldquo;Verwijderen&rdquo;.&nbsp; </p>
        <p class="MsoNormal">Ben je klaar?&nbsp; Dan kan  je rechtsboven op de knop drukken met je linkermuisknop op de foto&rsquo;s toe te  voegen aan uw album.&nbsp; </p>
        <p align="center" class="MsoNormal"><img border = "1" src="images/help/fotoalbum12.jpg" width="450" height="317" /></p>
        <p class="MsoNormal">Nu krijg je een schermpje te zien zoals op de volgende foto.</p>
        <p align="center" class="MsoNormal"><img border = "1" src="images/help/fotoalbum13.jpg" width="450" height="158" /></p>
        <p class="MsoNormal">Het toevoegen van foto&rsquo;s op een album duurt namelijk  eventjes.&nbsp; Dit komt omdat de foto&rsquo;s  moeten worden verstuurd naar internet.&nbsp;  Net zoals het een tijdje duurt om een grote e-mail te versturen, zo  duurt het ook eventjes om foto&rsquo;s te versturen naar het internet voor een  fotoalbum.&nbsp; Afhankelijk van de snelheid  van je internet en van de grootte en aantal van je foto&rsquo;s kan dit enkele  seconden duren, of zelfs enkele uren.&nbsp; Je  ziet de computer aftellen.&nbsp; Intussen tijd  kan je op een andere website surfen, in een ander programma bezig zijn of  gewoon iets in het huishouden doen.&nbsp; </p>
        <p class="MsoNormal">Wanneer de computer klaar is, dan krijg je hiervoor een  melding.</p>
        <p align="center" class="MsoNormal"><img border = "1" src="images/help/fotoalbum14.jpg" width="450" height="239" /></p>
        <p class="MsoNormal">Klik met je linkermuisknop op &ldquo;OK&rdquo;.&nbsp; Je wordt nu naar een nieuwe pagina  gestuurd.&nbsp; Eigenlijk is het nu al  klaar!&nbsp; Al je foto&rsquo;s staan in het  fotoalbum.</p>
        <p class="MsoNormal">Maar je hebt nu nog de mogelijkheid om een titel te geven  aan elk van de foto&rsquo;s, sleutelwoorden toe te voegen en eventueel een  omschrijving.&nbsp; Dat is niet  verplicht.&nbsp; Je kan dat doen bij enkele  foto&rsquo;s of allemaal, of natuurlijk helemaal niet.&nbsp; Het kan echter wel nuttig zijn voor zowel  jezelf als de mensen die je foto&rsquo;s gaan bekijken om er een titel of een woordje  uitleg bij te zetten.<br />
        Tevens is het mogelijk om de foto&rsquo;s te draaien indien ze niet goed staan.&nbsp; Heb je bijvoorbeeld een verticale foto  genomen, maar ligt ze plat, dan kan je dit wijzigen door op de pijlen onder de  foto te klikken.</p>
        <p class="MsoNormal">Het ingeven van trefwoorden doe je door woorden in te geven  die overeenstemmen met de foto.&nbsp; Zo kan  je voor een boomstam bijvoorbeeld &ldquo;boom, boomstam, boomschors, bos, park,  natuur, boswandeling&rdquo; ingeven.&nbsp; Deze  trefwoorden worden achteraf gebruikt om de zoekfunctie aan te sturen.&nbsp; Indien iemand bij je fotoalbum iets wil  zoeken, kan het zo supersnel worden gevonden.</p>
        <p align="center" class="MsoNormal"><img border = "1" src="images/help/fotoalbum15.jpg" width="450" height="337" /></p>
        <p class="MsoNormal">Het is dus niet verplicht, maar wel aangeraden.&nbsp; Ben je klaar?&nbsp;  Dan klik je op de knop &ldquo;Verzenden&rdquo;.&nbsp; </p>
        <p class="MsoNormal">Je krijgt nu een bevestiging te zien.&nbsp; Je kan je fotoalbum al onmiddellijk bekijken  door op &ldquo;Bekijk album&rdquo; te klikken met je linkermuisknop.&nbsp; </p>
        <p align="center" class="MsoNormal"><img border = "1" src="images/help/fotoalbum16.jpg" width="450" height="371" /></p>
        <p class="MsoNormal">Wil je nog foto&rsquo;s toevoegen, dan kan je op &ldquo;Meer foto&rsquo;s  toevoegen&rdquo; klikken.&nbsp; Wil je nog een omschrijving  of titel wijzigen, of misschien een foto terug verwijderen, dan kan dat via  &ldquo;foto&rsquo;s wijzigen/verwijderen&rdquo;.&nbsp; </p>
        <p class="MsoNormal">Ik klik een keer op &ldquo;Bekijk album&rdquo;, en krijg netjes alle  foto&rsquo;s te zien, zoals je ook op volgende foto kan zien.</p>
        <p align="center" class="MsoNormal"><img border = "1" src="images/help/fotoalbum17.jpg" width="450" height="249" /></p>
        <p>&nbsp;</p>
        <p>Herhaal de stappen in dit album opnieuw voor je volgende nieuwe albums. Op SeniorenNet is er geen beperking van aantal albums per gebruiker, je kan het dus zo vaak doen als je zelf wenst!<br />
        </p>
      <p align="left">&nbsp;</p></td>
    </tr>
  </table>
</div>
<br></td>

    </div> <!-- /content-->
    <div id="widgets" class="columns">
<div id="fb-root"></div>
<script>(function (d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id))
            return;
        js = d.createElement(s);
        js.id = id;
        js.src = "//connect.facebook.net/nl_BE/sdk.js#xfbml=1&version=v2.6&appId=513131492093790";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>

<div class="fb-page mt1 mb1" data-href="https://www.facebook.com/seniorennet" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><div class="fb-xfbml-parse-ignore"><blockquote cite="https://www.facebook.com/seniorennet"><a href="https://www.facebook.com/seniorennet">Seniorennet</a></blockquote></div></div>


      <div id="printpage" class="widget-item">
        <div id="printpage-inner" class="padding-inner">
          <table width="100%">
            <tr>
              <td valign="middle"><a href="#" onClick="
              var str = document.URL;
              var url_check = str.indexOf( '?' );
              if ( url_check != -1 ) {
                 var urlke = window.location + '&afdrukken=ja';
              }
              else
              {
                var urlke = window.location + '?afdrukken=ja';
              }
              newwindow=window.open(urlke,'Afdrukken','height=800,width=700,menubar=no,scrollbars=yes,resizable=yes,status=no,toolbar=no,menubar=no');
              if (window.focus) {newwindow.focus()} return false;">Print pagina</a></td>
              <td valign="middle" align="right" width="20%"><img src="//www.seniorennet.be/images/icons/print.gif" alt="Pagina afdrukken" /></td>
            </tr>
          </table>
        </div>
      </div>
      <div class="widget-item">
        <div class="right-promobox">
          <div id='div-gpt-ad-1462356799442-3'>
            <script type='text/javascript'>
            googletag.cmd.push(function() { googletag.display('div-gpt-ad-1462356799442-3'); });
            </script>
          </div>
          <div id='div-gpt-ad-1462356799442-4'>
            <script type='text/javascript'>
            googletag.cmd.push(function() { googletag.display('div-gpt-ad-1462356799442-4'); });
            </script>
          </div>
          <div style="margin: 1em 0;" id='div-gpt-ad-1462356799442-1'>
            <script type='text/javascript'>
            googletag.cmd.push(function() { googletag.display('div-gpt-ad-1462356799442-1'); });
            </script>
          </div>
          <div id='div-gpt-ad-1462356799442-2'>
            <script type='text/javascript'>
            googletag.cmd.push(function() { googletag.display('div-gpt-ad-1462356799442-2'); });
            </script>
          </div>
        </div>
      </div>

      
      
      <div id="poll-wrap">
          
      </div>
      <script type="text/javascript">$('#poll-wrap').load('//www.seniorennet.be/polls/ajax-view');</script>

    </div>

<div id="pagefooter" class="columns">
  <div id="bookmarks">
    <div id="bookmarks-inner">
      <img src="https://www.facebook.com/favicon.ico" alt="facebook" />
      <a href="#" onclick="window.open('https://www.facebook.com/sharer/sharer.php?u='+encodeURIComponent(location.href),'facebook-share-dialog','width=626,height=436');return false;">
        Deel op Facebook
      </a>
    </div>
  </div>

  <a href="https://www.seniorennet.be/pages/redactie#abonneren">
    <div class="nb-inschrijven-footer">
      <form name="input" action="https://www.seniorennet.be/pages/redactie#abonneren" method="post">
      	<input name="state" type="hidden" id="state" value="I">
      	<input name="fv" type="hidden" id="hdfv" value="1">
    	<input name="ruri" type="hidden" id="hdruri" value="/handleiding_2_nieuw_album_fotos_toevoegen_fotoalbum.php">
        <input type="email" name="emailadres" class="input" placeholder="emailadres" onClick="window.open('https://www.seniorennet.be/pages/redactie#abonneren');">
        <input type="submit" value="" class="button">
      </form>
    </div>
  </a>


  <div id="support">
  	<div id="support-inner">
    	Een vraag of een probleem op SeniorenNet? <br />
  		Kijk dan in het <a href="http://www.seniorennet.be/Pages/Overige/support.php">Helpcentrum (klik hier)</a>.
      Als je het daar niet kan vinden, helpen we je via e-mail op <a href="mailto:support@seniorennet.be">support@seniorennet.be</a>.
  	</div>
  </div>

</div>
</div>     
    

  <div id="footer_bottom"> Copyright &copy; 2001-2021 SeniorenNet - Alle rechten voorbehouden - 
  <a href="https://www.seniorennet.be/Pages/Overige/advert.php">Adverteren</a> | 
  <a href="https://www.seniorennet.be/Pages/Overige/support.php"  rel="nofollow">Contacteer ons</a> | 
  <a href="https://www.seniorennet.be/page/info/logo">Logo SeniorenNet</a> | 
  <a href="https://www.seniorennet.be/Pages/Overige/partners.php">Partners</a> | 
  <a href="https://www.seniorennet.be/Pages/Overige/over_ons.php">Over ons</a> | 
  <a href="https://www.seniorennet.be/Pages/Overige/privacy.php"  rel="nofollow">Privacy</a> | 
  <a href="https://www.seniorennet.be/page/info/disclaimer"  rel="nofollow">Disclaimer</a>

    </div>
  <div class="clear"></div>
</div>

<div class="clear"></div>


<script src="https://www.seniorennet.be/cache_js/foundation.js?v2018"></script>
<script type="text/javascript">
$(function(){
  $(document).foundation();
  $(window).scroll(function() {
    if ($(this).scrollTop() > 56){  
      $('#main-menubar').addClass("is-stuck");
      $('#main-menubar').removeClass("is-at-top");
      $("#main-menubar").css({
        'width': ($("#user-menubar").width() + 'px')
      });
      $("#user-menubar").css({
        'margin-bottom': ($("#main-menubar").height() + 'px')
      });
    } else{
      $('#main-menubar').addClass("is-at-top");
      $('#main-menubar').removeClass("is-stuck");
      $("#user-menubar").css({
        'margin-bottom': '0px'
      });
    }
  });
});
</script>

<!--[if lte IE 9]>
<script>
    var $buoop = {c:2,l:'nl'};
    function $buo_f(){
     var e = document.createElement("script");
     e.src = "//browser-update.org/update.min.js";
     document.body.appendChild(e);
    };
    try {document.addEventListener("DOMContentLoaded", $buo_f,false)}
    catch(e){window.attachEvent("onload", $buo_f)}


    $('.has-submenu').hover(function () {
        var left = $(this).offset().left - 245;
        $(this).find('.submenu').addClass('vertical').addClass('js-dropdown-active').css('left', left);
    }, function () {
        $(this).find('.submenu').removeClass('js-dropdown-active');
    });
</script>
<![endif]-->


<!--[if lt IE 9]>
    <script src="/js/ie8.js"></script>
<![endif]-->
</body>
</html>
</body>
</html>
