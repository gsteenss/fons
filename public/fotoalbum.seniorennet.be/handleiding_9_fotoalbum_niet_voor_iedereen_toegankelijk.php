</html>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-15" />
    <meta http-equiv="content-language" content="nl" />
            <meta name="Author" content="Seniorennet" />
    <meta http-equiv="Expires" content="Fri, 22 Jan 2021 17:15:18 CET" />
    <meta http-equiv="Pragma" content="no-cache" />
    <meta http-equiv="Cache-Control" content="no-cache" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    
    <title>SeniorenNet - Voor mensen met levenservaring en levenswijsheid</title>
    
          <!-- CSS NIEUWE LAYOUT -->


    <link rel="stylesheet" href="//www.seniorennet.be/css/print.css" type="text/css" media="print" />
    <link type="text/css" rel="stylesheet" href="//www.seniorennet.be/css/style.css?v20170717" />
    <link type="text/css" rel="stylesheet" href="//www.seniorennet.be/css/detailpagina.css" />
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
<link type="text/css" rel="stylesheet" href="https://www.seniorennet.be/css/sennet.css?v7" />


    <script src="https://www.google.com/jsapi?key=ABQIAAAA3sU-PhfOZtZRP_7JAqF6QRTBfbnnDyDqgyIAPZ5-G9HwiDjFoBTLf_CQBpr7R0r-7YUMhhY23aF0xQ" type="text/javascript"></script>

    <!--[if lt IE 9]>
        <link type="text/css" rel="stylesheet" href="https://www.seniorennet.be/cache_css/ie8.css?v1" />
            <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
            <script type="text/javascript" src="https://www.seniorennet.be/cache_js/ie8-head.js"></script>
    <![endif]-->
    <!--[if gt IE 8]><!-->
          <script src="//ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
        <!--<![endif]-->
    <script type="text/javascript" src="https://www.seniorennet.be/js/sennet.js"></script>

    
    
    

     
    

<script async='async' src='https://www.googletagservices.com/tag/js/gpt.js'></script>
<script>
    var googletag = googletag || {};
    googletag.cmd = googletag.cmd || [];
</script>

<script type='text/javascript'>
  googletag.cmd.push(function() {
    googletag.defineSlot('/8188103/SENNET_ROS_CONTENT1', [[300, 600], [300, 250]], 'div-gpt-ad-1462356799442-1').addService(googletag.pubads()).setCollapseEmptyDiv(true);
    googletag.defineSlot('/8188103/SENNET_ROS_CONTENT2', [[300, 600], [300, 100], [300, 250]], 'div-gpt-ad-1462356799442-2').addService(googletag.pubads()).setCollapseEmptyDiv(true);
    googletag.defineSlot('/8188103/SENNET_ROS_TOP', [[995, 125], [995, 123], [728, 90], [970, 90], [970, 250]], 'div-gpt-ad-1462356799442-0').addService(googletag.pubads()).setCollapseEmptyDiv(true);
    googletag.defineSlot('/8188103/SN_BE_Computertips_Logo', '[150x125]', 'div-gpt-ad-1462356799442-3').addService(googletag.pubads()).setCollapseEmptyDiv(true);
    googletag.defineSlot('/8188103/SN_BE_Computertips_Rectangle_Partner', '[150x125]', 'div-gpt-ad-1462356799442-4').addService(googletag.pubads()).setCollapseEmptyDiv(true);
    googletag.pubads().setTargeting('rubriek', ['old']);
    googletag.pubads().enableSingleRequest();
    googletag.enableServices();
  });

  function GA_googleFillSlot()
  {
    console.log('oldGAMTag');
  }
</script>
    
        
    </head>
<body  >
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-1117128-1', 'auto');
  ga('require', 'linkid', 'linkid.js');
  ga('require', 'displayfeatures');
  ga('send', 'pageview');

  var trackOutboundLink = function(url) {
    ga('send', 'event', 'outbound', 'click', url, {'hitCallback':
      function () {document.location = url;}
    });
  }

</script>

<script defer src="https://use.fontawesome.com/releases/v5.0.6/js/all.js"></script>

<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/nl_NL/all.js#xfbml=1";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>


   <!-- NIEUWE HEADER -->
<div id="page" class="row main-container">
  <div class="columns xlarge-12 small-12">
    <div id="header" class="row">
      <div class="user-menu row show-for-large" id="user-menubar">

        <div class="columns medium-3  menu-text main-logo">
            <a href="https://www.seniorennet.be/"><img src="https://www.seniorennet.be/img/layout/logo-seniorennet.svg?1516978662" alt="seniorennet" title="seniorennet" class="sn-logo hide-for-small-only" onerror="this.src=&#039;http:\/\/www.seniorennet.be/img/layout/logo-seniorennet.png&#039;"/></a>    
        </div>

        <div class="columns medium-5 xlarge-6">
            <form id="searchbox_003717546627450800428:obdjujbywam" action="https://www.seniorennet.be/zoeken/">
                <input value="003717546627450800428:obdjujbywam" name="cx" type="hidden"/>
                <input value="FORID:11" name="cof" type="hidden"/>
                <div class="search-button input-group">
                    <input id="q" name="q" type="search" class="input-group-field" placeholder="Zoeken">
                    <div class="input-group-button">
                        <button type="submit" name="sa" class="button"><i class="fas fa-search" data-fa-transform="left-6"></i></button>
                        <input type="submit" value="" />
                    </div>
                </div>
            </form>
        </div>
        <div class="columns medium-3 text-right" style="margin-left: -10px;">
            <ul>
                          <li><a class="" href="https://www.seniorennet.be/login"> <i class="hide-for-large fas fa-user"></i>Aanmelden</a></li>
              <li><a class="red" href="https://www.seniorennet.be/register">Registreren</a></li>
                          </ul>
        </div>
      </div>




      <div class="top-bar is-at-top" id="main-menubar">

        <div class="top-bar-left">
            <ul class="horizontal dropdown menu" data-dropdown-menu>
                <li id="sub-logo">
                    <a href="/"><img src="https://www.seniorennet.be/img/layout/logo-seniorennet.svg?1516978662" alt="seniorennet" title="seniorennet" onerror="this.src=&#039;https:\/\/www.seniorennet.be/img/layout/logo-seniorennet.png&#039;"/></a>
                </li>
                <li class="has-submenu">
                    <a href="https://www.seniorennet.be/nieuws">Nieuws</a>
                </li>
                <li class="has-submenu">
                    <a href="https://www.seniorennet.be/rubriek/gezondheid">Gezondheid</a>

                </li>
                <li class="has-submenu">
                    <a href="https://www.seniorennet.be/rubriek/wonen">Wonen</a>

                </li>
                <li class="has-submenu">
                    <a href="https://www.seniorennet.be/rubriek/geld-en-werk">Geld &amp; Recht</a>

                </li>
                <li class="has-submenu">
                    <a href="https://www.seniorennet.be/rubriek/toerisme">Toerisme</a>

                </li>
                <li class="has-submenu">
                    <a href="#">Vrije Tijd</a>

                    <ul class="submenu menu is-dropdown-submenu double" data-submenu>
                        <li><a href="https://www.seniorennet.be/Pages/grappig_schattig/">Grappig of Schattig</a></li>
                        <li><a href="/page/info/agenda">Activiteiten</a></li>
                        <li><a href="http://administratie.seniorennet.be/Nieuwsbrief/index_sennet_plezier.php">Dagelijks Grapje</a></li>
                        <li><a href="https://www.seniorennet.be/Pages/Vrije_tijd/spelletjes.php">Spelletjes</a></li>
                        <li><a href="https://www.seniorennet.be/Pages/Vrije_tijd/vrije_tijd.php">Vrije Tijd</a></li>
                        <li><a href="https://www.seniorennet.be/Pages/Vrije_tijd/Quiz/quiz.php">Quiz</a></li>
                        <li><a href="https://www.seniorennet.be/Pages/Visitekaartjes/visitekaartjes.php">Visitekaartjes</a></li>
                        <li><a href="https://www.seniorennet.be/ecards/">E-cards</a></li>
                        <li><a href="http://fotografie.seniorennet.be/">Fotografie</a></li>
                        <li><a href="https://www.seniorennet.be/rubriek/culinair/">Culinair</a></li>
                        <li><a href="https://www.seniorennet.be/rubriek/tuinkriebels/">Tuinkriebels</a></li>
                    </ul>
                </li>
                <li class="has-submenu">
                    <a href="#">Community</a>

                    <ul class="submenu menu is-dropdown-submenu " data-submenu>
                        <li><a href="https://www.seniorennet.be/pages/redactie">Nieuwsbrief</a></li>
                        <li><a href="https://www.seniorennet.be/forum/">Forum</a></li>
                        <li><a href="http://blog.seniorennet.be/">Blogs</a></li>
                        <li><a href="https://www.seniorennet.be/Pages/Vrije_tijd/chatbox.php">Chatbox</a></li>
                        <li><a href="https://www.seniorennet.be/Pages/Mailgroepen/mailgroepen.php">Mailgroepen</a></li>
                        <li><a href="http://fotoalbum.seniorennet.be/">Foto-album</a></li>
                        <li><a href="https://www.seniorennet.be/Pages/Zoeken/zoeken.php">Zoekertjes</a></li>
                        <li><a href="http://webmail.seniorennet.be/index.php">Webmail</a></li>
                    </ul>
                </li>
                <li class="has-submenu">
                    <a href="#">Nuttig</a>

                    <ul class="submenu menu is-dropdown-submenu " data-submenu>
                        <li><a href="https://www.seniorennet.be/pages/nuttige-links">Nuttige Info</a></li>
                        <li><a href="https://www.seniorennet.be/Pages/tips/tips.php">Tips</a></li>
                        <li><a href="https://www.seniorennet.be/Pages/Weer/weer.php">Weer</a></li>
                        <li><a href="https://www.seniorennet.be/Pages/Weer/buienradar.php">Buienradar</a></li>
                        <li><a href="https://www.seniorennet.be/Pages/Auto/verkeersinfo.php">Verkeersinfo</a></li>
                        <li><a href="https://www.seniorennet.be/rubriek/thuis-op-internet/">Thuis op Internet</a></li>
                        <li><a href="https://www.seniorennet.be/Pages/Thuis_op_internet/computerhulp.php">Computerhulp</a></li>
                        <li><a href="https://www.seniorennet.be/Pages/Vrije_tijd/omrekenen.php">Omrekenen</a></li>
                    </ul>
                </li>




            </ul>
        </div>

        <div class="top-bar-right">
            <ul class="dropdown menu" data-dropdown-menu id="menu-topicon" >
                <li>
                    <a href="/zoeken"><i class="fas fa-search fa-lg" data-fa-transform="right-1"></i></a>
                </li>
                <li>
                    <a href="#"><i class="fas fa-user-circle fa-lg" data-fa-transform="right-1"></i></a>
                    <ul class="submenu menu is-dropdown-submenu" data-submenu>
                        <li><a class="" href="https://www.seniorennet.be/login"> Aanmelden</a></li>
                        <li><a href="https://www.seniorennet.be/login">Registreren</a></li>
                    </ul>
                </li>
            </ul>
        </div>

    </div>



</div><!-- #header -->

    <div class="columns content">
      <!-- CONTENT -->




 <!-- EINDE NIEUWE HEADER -->





  </div>
  <div id="wrapper">
    <div id="wrapper-inner">

      <div id="advertisement" class="top">
          <div id="advertisement-inner">
            
            <div id='div-gpt-ad-1462356799442-0'>
            <script type='text/javascript'>
            googletag.cmd.push(function() { googletag.display('div-gpt-ad-1462356799442-0'); });
            </script>
            </div>  
            <div class="clear"></div>
          </div>
        </div>
      <div id="wrapper-inner-content">
      <div id="main" class="row">
    <div class="" id="content">

    <style type="text/css">
<!--
p.MsoNormal {
margin-top:0cm;
margin-right:0cm;
margin-bottom:10.0pt;
margin-left:0cm;
}
-->
</style>
<div class="body"> 
  <table width="100%" border="0" cellspacing="5" cellpadding="0">
    <tr> 
      <td valign="top"> 
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr> 
            <td bgcolor="#BABA74" height="20"><font color="#003333"> Waar vandaan: <a href="index.php">Foto albums</a> &gt; <a href="help.php">Help</a> &gt; Foto-album afschermen </font></td>
          </tr>
        </table>
        <h1 align="left"><strong>Een foto-album dat niet voor iedereen toegankelijk is </strong></h1>
        <p class="MsoNormal">Het is perfect mogelijk dat je foto's in een album steekt dat enkel jij kan openen en niemand anders. Het is ook mogelijk om de foto's aan bepaalde familieleden of vrienden te tonen; maar enkel wie jij specifiek opgeeft. Vooral voor priv&eacute; foto's is dit een handige functie!</p>
        <p class="MsoNormal">Het is  geen probleem om bijvoorbeeld je ouders het ene album wel te laten zien en je kinderen niet, terwijl je een ander album wel je kinderen toegang toe geeft, maar niet je ouders.</p>
        <h3 class="MsoNormal">STAP 1 a) - U heeft nog geen album: FOTOALBUM AANMAKEN</h3>
        <p class="MsoNormal">Indien je nog geen foto-album hebt aangemaakt voor de private foto's, dan doe je dit eerst. </p>
        <ol>
          <li>Surf naar <a href="http://fotoalbum.seniorennet.be/">http://fotoalbum.seniorennet.be/</a> en log in.<br />
          &nbsp;</li>
          <li>Klik op &quot;Nieuw fotoalbum aanmaken&quot;.<br />
          &nbsp;</li>
          <li>Geef een titel op, kies het adres van het album en geef de categorie op.<br />
          &nbsp;</li>
          <li>Bij &quot;Zichtbaarheid&quot; komt nu het belangrijke punt. Standaard kan iedereen je foto's zien. Maar dat wil je in dit geval niet.<br />
            <br />
          Klik &eacute;&eacute;n van de drie mogelijkheden aan: 
            <ol>
              <li>Enkel vrienden &eacute;n familie mogen het album zien </li>
              <li>Enkel vrienden mogen het album zien</li>
              <li>Enkel familie mag het album zien</li>
            </ol>
          Je kan later nog aanpassen welke vrienden en familie nu precies toegang krijgen. Dat kan per album apart geregeld worden indien gewenst.<br />
          &nbsp;<br />
          </li>
          <li>Klik op &quot;Verzenden&quot; om het album aan te maken. <br />
           &nbsp;</li>
          <li>Voeg de foto's toe aan je album. <a href="handleiding_2_nieuw_album_fotos_toevoegen_fotoalbum.php">Hoe je dat doet, kan je terugvinden op deze pagina (klik hier).</a> </li>
        </ol>
        <h3>STAP 1  b) - U heeft al een album, maar wil dat NU AFSCHERMEN</h3>
        <p>Als u reeds een album (met eventueel al foto's) hebt aangemaakt en wil dit achteraf afschermen voor vrienden en familie, doet u dit als volgt.</p>
        <ol>
          <li>Surf naar <a href="http://fotoalbum.seniorennet.be/">http://fotoalbum.seniorennet.be/</a> en log in.<br />
          &nbsp;</li>
          <li>Klik op de link &quot;Bestaand album aanpassen&quot;.  <br />
            &nbsp;<br />
          </li>
          <li>Klik op het album dat u wenst af te schermen.<br />
          &nbsp;</li>
          <li>Klik nu in het menu op &quot;Instellingen voor dit album&quot;.<br />
          &nbsp;</li>
          <li>Ga naar beneden op de pagina die verschijnt. Ongeveer halverwege zie je onder de titel &quot;Soort album&quot; staan: &quot;Wie mag het album zien?&quot;.<br />
          &nbsp;</li>
          <li>Klik hier de gewenste keuze aan.            
            <ol>
              <li>Enkel vrienden &eacute;n familie mogen het album zien </li>
              <li>Enkel vrienden mogen het album zien</li>
              <li>Enkel familie mag het album zien<br />
              &nbsp;</li>
            </ol>
          </li>
          <li>Ga helemaal naar onder de pagina en klik op &quot;Verzenden&quot; om de instellingen te bewaren. </li>
        </ol>
        <h3 class="MsoNormal">STAP 2 - Uw vrienden/familie toevoegen</h3>
        <p class="MsoNormal">Je album is nu voor niemand toegankelijk, behalve voor uzelf en voor de vrienden/familie (afhankelijk van de gekozen optie). <br />
        De computer kan natuurlijk niet zomaar weten wie uw vrienden en familie zijn. </p>
        <p class="MsoNormal">Je kan hen eenvoudig uitnodigen om zich te registeren. Eenmaal geregistreerd kunnen zij dan onmiddellijk in uw albums.</p>
        <p class="MsoNormal">Om hen uit te nodigen doe je dit als volgt:</p>
        <ul>
          <li>Log in op http://fotoalbum.seniorennet.be</li>
          <li>Klik op &quot;Mijn vrienden/familie&quot;</li>
          <li>Klik op de link die staat onder de titel &quot;Vriend of familielid uitnodigen&quot;</li>
          <li>Geef het e-mail adres in van je vriend/familielid. Geef op om wie het gaat (vriend/familie) en geef eventueel een persoonlijk tekstje in.</li>
          <li>Klik op &quot;Verzenden&quot;.</li>
          <li>Nadat zij zich geregistreerd hebben kunnen ze onmiddellijk je albums bekijken, zonder dat je hiervoor nog iets moet doen.</li>
        </ul>
        <p class="MsoNormal">Indien ze al geregistreerd zijn op de SeniorenNet Fotoalbums, kan je ze al onmiddellijk toevoegen.</p>
        <p class="MsoNormal">        Het is eenvoudig: </p>
        <ol>
          <li>Log in</li>
          <li>Klik op &quot;Mijn vrienden / familie&quot;</li>
          <li>Geef de gebruikersnaam bij &quot;Voeg vriend toe&quot; of &quot;Voeg familie toe&quot; en klik op   de knop &quot;Verzenden&quot;. </li>
        </ol>
        <p>Een vriend/familie hoef je maar eenmalig toe te voegen, daarna wordt dit onthouden. Indien je dit dus al eens eerder hebt gedaan, hoef je dit niet elke keer opnieuw te doen. Indien een nieuwe vriend of familielid toegang moet krijgen moet je die uiteraard achteraf nog wel toevoegen.</p>
        <h3>STAP 3 - Eventuele vrienden/familie uitsluiten op een specifiek album</h3>
        <p>Wie je toevoegt als vriend of familie krijgt toegang tot alle albums die je opgaf voor enkel vrienden/familie toegankelijk te zijn. </p>
        <p>Het kan voorkomen dat je bepaalde vrienden niet wil toelaten op een bepaald album, terwijl je op een ander album weer andere vrienden wil toelaten. Of bijvoorbeeld het ene album enkel toegankelijk maken voor je kinderen en niet je ouders, terwijl een ander album enkel voor je ouders en niet voor je kinderen toegankelijk mag zijn.</p>
        <p>Je kan dit doen door naar de instellingen van het specifieke album te gaan en op dat album een aantal vrienden/familie toevoegen om geblokkeerd te worden. Indien je een album bijvoorbeeld enkel toegankelijk maakt voor familie, dan kan iedereen van je familie erin. Wil je bvb. je kinderen echter niet toelaten op dat specifieke album, dan voeg je je kinderen toe bij de geblokkeerde gebruikers op dat specifieke album.</p>
        <p>Je doet dit als volgt:</p>
        <ol>
          <li>Surf naar <a href="http://fotoalbum.seniorennet.be/">http://fotoalbum.seniorennet.be/</a> en log in.<br />
          &nbsp;</li>
          <li>Klik op de link &quot;Bestaand album aanpassen&quot;. <br />
  &nbsp;<br />
          </li>
          <li>Klik op het album dat u wenst te wijzigen.<br />
&nbsp;</li>
          <li>Klik op &quot;Blokkeer gebruikers op DIT album&quot;   <br />
          &nbsp;</li>
          <li>Je krijgt nu een overzicht van alle gebruikers die toegang hebben tot dit specifieke foto-album. <br />
          Klik op het vierkantje voor de gebruikersnaam van de personen die je g&eacute;&eacute;n toegang wil geven. De mensen waarvan je dus de toegang van wil ontzeggen moet je dus aanklikken.<br />
&nbsp;</li>
          <li>Om je wijzigingen te bewaren klik je bovenaan of onderaan op de pagina op de knop &quot;Verzenden&quot;.</li>
        </ol>
        <p>Je kan op elk moment terug naar deze instellingen gaan en een bepaalde persoon terug toegang geven (door het vierkantje terug uit te vinken), of om nieuwe mensen toe te voegen die geen toegang meer mogen krijgen.</p>
        <p>Het systeem is volledig flexibel en de instellingen kunnen per album anders zijn. Blokkeer je iedereen, dan krijgt niemand nog toegang, behalve jijzelf.  </p>
        <p class="MsoNormal">&nbsp;</p>
        <p class="MsoNormal"><br />
        </p>
      <p align="left">&nbsp;</p></td>
    </tr>
  </table>
</div>
<br></td>

    </div> <!-- /content-->
    <div id="widgets" class="columns">
<div id="fb-root"></div>
<script>(function (d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id))
            return;
        js = d.createElement(s);
        js.id = id;
        js.src = "//connect.facebook.net/nl_BE/sdk.js#xfbml=1&version=v2.6&appId=513131492093790";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>

<div class="fb-page mt1 mb1" data-href="https://www.facebook.com/seniorennet" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><div class="fb-xfbml-parse-ignore"><blockquote cite="https://www.facebook.com/seniorennet"><a href="https://www.facebook.com/seniorennet">Seniorennet</a></blockquote></div></div>


      <div id="printpage" class="widget-item">
        <div id="printpage-inner" class="padding-inner">
          <table width="100%">
            <tr>
              <td valign="middle"><a href="#" onClick="
              var str = document.URL;
              var url_check = str.indexOf( '?' );
              if ( url_check != -1 ) {
                 var urlke = window.location + '&afdrukken=ja';
              }
              else
              {
                var urlke = window.location + '?afdrukken=ja';
              }
              newwindow=window.open(urlke,'Afdrukken','height=800,width=700,menubar=no,scrollbars=yes,resizable=yes,status=no,toolbar=no,menubar=no');
              if (window.focus) {newwindow.focus()} return false;">Print pagina</a></td>
              <td valign="middle" align="right" width="20%"><img src="//www.seniorennet.be/images/icons/print.gif" alt="Pagina afdrukken" /></td>
            </tr>
          </table>
        </div>
      </div>
      <div class="widget-item">
        <div class="right-promobox">
          <div id='div-gpt-ad-1462356799442-3'>
            <script type='text/javascript'>
            googletag.cmd.push(function() { googletag.display('div-gpt-ad-1462356799442-3'); });
            </script>
          </div>
          <div id='div-gpt-ad-1462356799442-4'>
            <script type='text/javascript'>
            googletag.cmd.push(function() { googletag.display('div-gpt-ad-1462356799442-4'); });
            </script>
          </div>
          <div style="margin: 1em 0;" id='div-gpt-ad-1462356799442-1'>
            <script type='text/javascript'>
            googletag.cmd.push(function() { googletag.display('div-gpt-ad-1462356799442-1'); });
            </script>
          </div>
          <div id='div-gpt-ad-1462356799442-2'>
            <script type='text/javascript'>
            googletag.cmd.push(function() { googletag.display('div-gpt-ad-1462356799442-2'); });
            </script>
          </div>
        </div>
      </div>

      
      
      <div id="poll-wrap">
          
      </div>
      <script type="text/javascript">$('#poll-wrap').load('//www.seniorennet.be/polls/ajax-view');</script>

    </div>

<div id="pagefooter" class="columns">
  <div id="bookmarks">
    <div id="bookmarks-inner">
      <img src="https://www.facebook.com/favicon.ico" alt="facebook" />
      <a href="#" onclick="window.open('https://www.facebook.com/sharer/sharer.php?u='+encodeURIComponent(location.href),'facebook-share-dialog','width=626,height=436');return false;">
        Deel op Facebook
      </a>
    </div>
  </div>

  <a href="https://www.seniorennet.be/pages/redactie#abonneren">
    <div class="nb-inschrijven-footer">
      <form name="input" action="https://www.seniorennet.be/pages/redactie#abonneren" method="post">
      	<input name="state" type="hidden" id="state" value="I">
      	<input name="fv" type="hidden" id="hdfv" value="1">
    	<input name="ruri" type="hidden" id="hdruri" value="/handleiding_9_fotoalbum_niet_voor_iedereen_toegankelijk.php">
        <input type="email" name="emailadres" class="input" placeholder="emailadres" onClick="window.open('https://www.seniorennet.be/pages/redactie#abonneren');">
        <input type="submit" value="" class="button">
      </form>
    </div>
  </a>


  <div id="support">
  	<div id="support-inner">
    	Een vraag of een probleem op SeniorenNet? <br />
  		Kijk dan in het <a href="http://www.seniorennet.be/Pages/Overige/support.php">Helpcentrum (klik hier)</a>.
      Als je het daar niet kan vinden, helpen we je via e-mail op <a href="mailto:support@seniorennet.be">support@seniorennet.be</a>.
  	</div>
  </div>

</div>
</div>     
    

  <div id="footer_bottom"> Copyright &copy; 2001-2021 SeniorenNet - Alle rechten voorbehouden - 
  <a href="https://www.seniorennet.be/Pages/Overige/advert.php">Adverteren</a> | 
  <a href="https://www.seniorennet.be/Pages/Overige/support.php"  rel="nofollow">Contacteer ons</a> | 
  <a href="https://www.seniorennet.be/page/info/logo">Logo SeniorenNet</a> | 
  <a href="https://www.seniorennet.be/Pages/Overige/partners.php">Partners</a> | 
  <a href="https://www.seniorennet.be/Pages/Overige/over_ons.php">Over ons</a> | 
  <a href="https://www.seniorennet.be/Pages/Overige/privacy.php"  rel="nofollow">Privacy</a> | 
  <a href="https://www.seniorennet.be/page/info/disclaimer"  rel="nofollow">Disclaimer</a>

    </div>
  <div class="clear"></div>
</div>

<div class="clear"></div>


<script src="https://www.seniorennet.be/cache_js/foundation.js?v2018"></script>
<script type="text/javascript">
$(function(){
  $(document).foundation();
  $(window).scroll(function() {
    if ($(this).scrollTop() > 56){  
      $('#main-menubar').addClass("is-stuck");
      $('#main-menubar').removeClass("is-at-top");
      $("#main-menubar").css({
        'width': ($("#user-menubar").width() + 'px')
      });
      $("#user-menubar").css({
        'margin-bottom': ($("#main-menubar").height() + 'px')
      });
    } else{
      $('#main-menubar').addClass("is-at-top");
      $('#main-menubar').removeClass("is-stuck");
      $("#user-menubar").css({
        'margin-bottom': '0px'
      });
    }
  });
});
</script>

<!--[if lte IE 9]>
<script>
    var $buoop = {c:2,l:'nl'};
    function $buo_f(){
     var e = document.createElement("script");
     e.src = "//browser-update.org/update.min.js";
     document.body.appendChild(e);
    };
    try {document.addEventListener("DOMContentLoaded", $buo_f,false)}
    catch(e){window.attachEvent("onload", $buo_f)}


    $('.has-submenu').hover(function () {
        var left = $(this).offset().left - 245;
        $(this).find('.submenu').addClass('vertical').addClass('js-dropdown-active').css('left', left);
    }, function () {
        $(this).find('.submenu').removeClass('js-dropdown-active');
    });
</script>
<![endif]-->


<!--[if lt IE 9]>
    <script src="/js/ie8.js"></script>
<![endif]-->
</body>
</html>
</body>
</html>
