</html>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-15" />
    <meta http-equiv="content-language" content="nl" />
            <meta name="Author" content="Seniorennet" />
    <meta http-equiv="Expires" content="Fri, 22 Jan 2021 17:15:18 CET" />
    <meta http-equiv="Pragma" content="no-cache" />
    <meta http-equiv="Cache-Control" content="no-cache" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    
    <title>SeniorenNet - Voor mensen met levenservaring en levenswijsheid</title>
    
          <!-- CSS NIEUWE LAYOUT -->


    <link rel="stylesheet" href="//www.seniorennet.be/css/print.css" type="text/css" media="print" />
    <link type="text/css" rel="stylesheet" href="//www.seniorennet.be/css/style.css?v20170717" />
    <link type="text/css" rel="stylesheet" href="//www.seniorennet.be/css/interactief.css?v=2" />
    <link type="text/css" rel="stylesheet" href="//www.seniorennet.be/css/fotoalbum.css" />
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
<link type="text/css" rel="stylesheet" href="https://www.seniorennet.be/css/sennet.css?v7" />


    <script src="https://www.google.com/jsapi?key=ABQIAAAA3sU-PhfOZtZRP_7JAqF6QRTBfbnnDyDqgyIAPZ5-G9HwiDjFoBTLf_CQBpr7R0r-7YUMhhY23aF0xQ" type="text/javascript"></script>

    <!--[if lt IE 9]>
        <link type="text/css" rel="stylesheet" href="https://www.seniorennet.be/cache_css/ie8.css?v1" />
            <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
            <script type="text/javascript" src="https://www.seniorennet.be/cache_js/ie8-head.js"></script>
    <![endif]-->
    <!--[if gt IE 8]><!-->
          <script src="//ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
        <!--<![endif]-->
    <script type="text/javascript" src="https://www.seniorennet.be/js/sennet.js"></script>

    
    
    

     
    

<script async='async' src='https://www.googletagservices.com/tag/js/gpt.js'></script>
<script>
    var googletag = googletag || {};
    googletag.cmd = googletag.cmd || [];
</script>

<script type='text/javascript'>
  googletag.cmd.push(function() {
    googletag.defineSlot('/8188103/SENNET_ROS_CONTENT1', [[300, 600], [300, 250]], 'div-gpt-ad-1462356799442-1').addService(googletag.pubads()).setCollapseEmptyDiv(true);
    googletag.defineSlot('/8188103/SENNET_ROS_CONTENT2', [[300, 600], [300, 100], [300, 250]], 'div-gpt-ad-1462356799442-2').addService(googletag.pubads()).setCollapseEmptyDiv(true);
    googletag.defineSlot('/8188103/SENNET_ROS_TOP', [[995, 125], [995, 123], [728, 90], [970, 90], [970, 250]], 'div-gpt-ad-1462356799442-0').addService(googletag.pubads()).setCollapseEmptyDiv(true);
    googletag.defineSlot('/8188103/SN_BE_Fotoalbum_Logo', '[150x125]', 'div-gpt-ad-1462356799442-3').addService(googletag.pubads()).setCollapseEmptyDiv(true);
    googletag.defineSlot('/8188103/SN_BE_Fotoalbum_Rectangle_Partner', '[150x125]', 'div-gpt-ad-1462356799442-4').addService(googletag.pubads()).setCollapseEmptyDiv(true);
    googletag.pubads().setTargeting('rubriek', ['old']);
    googletag.pubads().enableSingleRequest();
    googletag.enableServices();
  });

  function GA_googleFillSlot()
  {
    console.log('oldGAMTag');
  }
</script>
    
        
    </head>
<body  >
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-1117128-1', 'auto');
  ga('require', 'linkid', 'linkid.js');
  ga('require', 'displayfeatures');
  ga('send', 'pageview');

  var trackOutboundLink = function(url) {
    ga('send', 'event', 'outbound', 'click', url, {'hitCallback':
      function () {document.location = url;}
    });
  }

</script>

<script defer src="https://use.fontawesome.com/releases/v5.0.6/js/all.js"></script>

<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/nl_NL/all.js#xfbml=1";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>


   <!-- NIEUWE HEADER -->
<div id="page" class="row main-container">
  <div class="columns xlarge-12 small-12">
    <div id="header" class="row">
      <div class="user-menu row show-for-large" id="user-menubar">

        <div class="columns medium-3  menu-text main-logo">
            <a href="https://www.seniorennet.be/"><img src="https://www.seniorennet.be/img/layout/logo-seniorennet.svg?1516978662" alt="seniorennet" title="seniorennet" class="sn-logo hide-for-small-only" onerror="this.src=&#039;http:\/\/www.seniorennet.be/img/layout/logo-seniorennet.png&#039;"/></a>    
        </div>

        <div class="columns medium-5 xlarge-6">
            <form id="searchbox_003717546627450800428:obdjujbywam" action="https://www.seniorennet.be/zoeken/">
                <input value="003717546627450800428:obdjujbywam" name="cx" type="hidden"/>
                <input value="FORID:11" name="cof" type="hidden"/>
                <div class="search-button input-group">
                    <input id="q" name="q" type="search" class="input-group-field" placeholder="Zoeken">
                    <div class="input-group-button">
                        <button type="submit" name="sa" class="button"><i class="fas fa-search" data-fa-transform="left-6"></i></button>
                        <input type="submit" value="" />
                    </div>
                </div>
            </form>
        </div>
        <div class="columns medium-3 text-right" style="margin-left: -10px;">
            <ul>
                          <li><a class="" href="https://www.seniorennet.be/login"> <i class="hide-for-large fas fa-user"></i>Aanmelden</a></li>
              <li><a class="red" href="https://www.seniorennet.be/register">Registreren</a></li>
                          </ul>
        </div>
      </div>




      <div class="top-bar is-at-top" id="main-menubar">

        <div class="top-bar-left">
            <ul class="horizontal dropdown menu" data-dropdown-menu>
                <li id="sub-logo">
                    <a href="/"><img src="https://www.seniorennet.be/img/layout/logo-seniorennet.svg?1516978662" alt="seniorennet" title="seniorennet" onerror="this.src=&#039;https:\/\/www.seniorennet.be/img/layout/logo-seniorennet.png&#039;"/></a>
                </li>
                <li class="has-submenu">
                    <a href="https://www.seniorennet.be/nieuws">Nieuws</a>
                </li>
                <li class="has-submenu">
                    <a href="https://www.seniorennet.be/rubriek/gezondheid">Gezondheid</a>

                </li>
                <li class="has-submenu">
                    <a href="https://www.seniorennet.be/rubriek/wonen">Wonen</a>

                </li>
                <li class="has-submenu">
                    <a href="https://www.seniorennet.be/rubriek/geld-en-werk">Geld &amp; Recht</a>

                </li>
                <li class="has-submenu">
                    <a href="https://www.seniorennet.be/rubriek/toerisme">Toerisme</a>

                </li>
                <li class="has-submenu">
                    <a href="#">Vrije Tijd</a>

                    <ul class="submenu menu is-dropdown-submenu double" data-submenu>
                        <li><a href="https://www.seniorennet.be/Pages/grappig_schattig/">Grappig of Schattig</a></li>
                        <li><a href="/page/info/agenda">Activiteiten</a></li>
                        <li><a href="http://administratie.seniorennet.be/Nieuwsbrief/index_sennet_plezier.php">Dagelijks Grapje</a></li>
                        <li><a href="https://www.seniorennet.be/Pages/Vrije_tijd/spelletjes.php">Spelletjes</a></li>
                        <li><a href="https://www.seniorennet.be/Pages/Vrije_tijd/vrije_tijd.php">Vrije Tijd</a></li>
                        <li><a href="https://www.seniorennet.be/Pages/Vrije_tijd/Quiz/quiz.php">Quiz</a></li>
                        <li><a href="https://www.seniorennet.be/Pages/Visitekaartjes/visitekaartjes.php">Visitekaartjes</a></li>
                        <li><a href="https://www.seniorennet.be/ecards/">E-cards</a></li>
                        <li><a href="http://fotografie.seniorennet.be/">Fotografie</a></li>
                        <li><a href="https://www.seniorennet.be/rubriek/culinair/">Culinair</a></li>
                        <li><a href="https://www.seniorennet.be/rubriek/tuinkriebels/">Tuinkriebels</a></li>
                    </ul>
                </li>
                <li class="has-submenu">
                    <a href="#">Community</a>

                    <ul class="submenu menu is-dropdown-submenu " data-submenu>
                        <li><a href="https://www.seniorennet.be/pages/redactie">Nieuwsbrief</a></li>
                        <li><a href="https://www.seniorennet.be/forum/">Forum</a></li>
                        <li><a href="http://blog.seniorennet.be/">Blogs</a></li>
                        <li><a href="https://www.seniorennet.be/Pages/Vrije_tijd/chatbox.php">Chatbox</a></li>
                        <li><a href="https://www.seniorennet.be/Pages/Mailgroepen/mailgroepen.php">Mailgroepen</a></li>
                        <li><a href="http://fotoalbum.seniorennet.be/">Foto-album</a></li>
                        <li><a href="https://www.seniorennet.be/Pages/Zoeken/zoeken.php">Zoekertjes</a></li>
                        <li><a href="http://webmail.seniorennet.be/index.php">Webmail</a></li>
                    </ul>
                </li>
                <li class="has-submenu">
                    <a href="#">Nuttig</a>

                    <ul class="submenu menu is-dropdown-submenu " data-submenu>
                        <li><a href="https://www.seniorennet.be/pages/nuttige-links">Nuttige Info</a></li>
                        <li><a href="https://www.seniorennet.be/Pages/tips/tips.php">Tips</a></li>
                        <li><a href="https://www.seniorennet.be/Pages/Weer/weer.php">Weer</a></li>
                        <li><a href="https://www.seniorennet.be/Pages/Weer/buienradar.php">Buienradar</a></li>
                        <li><a href="https://www.seniorennet.be/Pages/Auto/verkeersinfo.php">Verkeersinfo</a></li>
                        <li><a href="https://www.seniorennet.be/rubriek/thuis-op-internet/">Thuis op Internet</a></li>
                        <li><a href="https://www.seniorennet.be/Pages/Thuis_op_internet/computerhulp.php">Computerhulp</a></li>
                        <li><a href="https://www.seniorennet.be/Pages/Vrije_tijd/omrekenen.php">Omrekenen</a></li>
                    </ul>
                </li>




            </ul>
        </div>

        <div class="top-bar-right">
            <ul class="dropdown menu" data-dropdown-menu id="menu-topicon" >
                <li>
                    <a href="/zoeken"><i class="fas fa-search fa-lg" data-fa-transform="right-1"></i></a>
                </li>
                <li>
                    <a href="#"><i class="fas fa-user-circle fa-lg" data-fa-transform="right-1"></i></a>
                    <ul class="submenu menu is-dropdown-submenu" data-submenu>
                        <li><a class="" href="https://www.seniorennet.be/login"> Aanmelden</a></li>
                        <li><a href="https://www.seniorennet.be/login">Registreren</a></li>
                    </ul>
                </li>
            </ul>
        </div>

    </div>



</div><!-- #header -->

    <div class="columns content">
      <!-- CONTENT -->




 <!-- EINDE NIEUWE HEADER -->





  </div>
  <div id="wrapper">
    <div id="wrapper-inner">

      <div id="advertisement" class="top">
          <div id="advertisement-inner">
            
            <div id='div-gpt-ad-1462356799442-0'>
            <script type='text/javascript'>
            googletag.cmd.push(function() { googletag.display('div-gpt-ad-1462356799442-0'); });
            </script>
            </div>  
            <div class="clear"></div>
          </div>
        </div>
      <div id="wrapper-inner-content">
      <div id="main" class="row">
    <div class="" id="content">

    <style type="text/css">
<!--
p.MsoNormal {
margin-top:0cm;
margin-right:0cm;
margin-bottom:10.0pt;
margin-left:0cm;
}
-->
</style>
<div class="body"> 
  <table width="100%" border="0" cellspacing="5" cellpadding="0">
    <tr> 
      <td valign="top"> 
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr> 
            <td bgcolor="#BABA74" height="20"><font color="#003333"> Waar vandaan: <a href="index.php">Foto albums</a> &gt; <a href="help.php">Help</a> &gt; Foto-album statistieken </font></td>
          </tr>
        </table>
        <h1 align="left">Kan ik zien hoeveel keren mijn foto's werden gezien? </h1>
        <p class="MsoNormal">Jazeker! </p>
        <p class="MsoNormal">Je hebt statistieken over &aacute;l je albums heen, per album en zelfs per foto! </p>
        <h3 class="MsoNormal">Statistieken over al je albums samen</h3>
        <p class="MsoNormal">Om de statistieken te bekijken over al je albums heen, doe je dit als volgt:</p>
        <ol>
          <li>Surf naar <a href="http://fotoalbum.seniorennet.be/">http://fotoalbum.seniorennet.be/</a> en log in.<br />
          &nbsp;</li>
          <li>Klik in het menu op &quot;Algemene statistieken&quot; met je linkermuisknop.<br />
&nbsp;<img src="images/help/fotoalbum_statistieken_1.jpg" width="450" height="299" border="1" /><br />
          &nbsp;</li>
          <li>Je krijgt nu een pagina met alle statistieken!<br />
          <img src="images/help/fotoalbum_statistieken_2.jpg" width="450" height="451" border="1" /></li>
        </ol>
        <p>Op deze pagina krijg je bovenaan eerst algemene gegevens. Het totaal aantal albums dat je hebt, het aantal foto's, hoeveel paginaweergaves je in totaal al hebt ontvangen, hoeveel megabyte (MB) aan foto's je in totaal hebt toegevoegd, enz.</p>
        <p>Daaronder krijg je de statistieken per dag, per week en per maand te zien. Zo kan je de evolutie bekijken in de tijd. </p>
        <h3 class="MsoNormal">Statistieken per album</h3>
        <p class="MsoNormal">Je kan specifiek van je album bekijken hoeveel paginaweergaves er zijn geweest en hoeveel foto's er in dat album staan.</p>
        <p class="MsoNormal">Je kan deze eenvoudig opvragen door gewoon te surfen naar het album. Open het album (net zoals andere bezoekers dit zouden openen).</p>
        <p class="MsoNormal">Rechtsboven krijg je dan bij &quot;Album details&quot; de gegevens te zien.          </p>
        <p align="center" class="MsoNormal"><img src="images/help/fotoalbum_statistieken_3.jpg" width="450" height="439" border="1" /></p>
        <p align="left" class="MsoNormal">Wil je nog m&eacute;&eacute;r weten? Dat kan! </p>
        <ol>
          <li>Surf naar <a href="http://fotoalbum.seniorennet.be/">http://fotoalbum.seniorennet.be/</a> en log in.<br />
          &nbsp;</li>
          <li>Klik op &quot;Bestaand album aanpassen&quot;.<br />
          &nbsp;</li>
          <li>Klik het album aan waarvan je de statistieken wenst te kennen.<br />
          &nbsp;</li>
          <li>Klik in het menu dat verschijnt op   &quot;Statistieken album&quot;.<br />
          &nbsp;</li>
          <li>Je krijgt nu gedetailleerde statistieken te zien van het album, met enerzijds algemene statistieken als specifiek  per dag, week en per maand.  </li>
        </ol>
        <h3 class="MsoNormal">Statistieken per foto   </h3>
        <p>Hoeveel keer werd een specifieke foto bekeken? Je kan dat zeer eenvoudig te weten komen. Open gewoon je foto-album, open vervolgens de foto en daar staat de informatie! </p>
        <p align="left">In de rechtse kolom kan je deze bij &quot;Statistieken&quot; vinden. </p>
        <p align="center"><img src="images/help/fotoalbum_statistieken_4.jpg" width="450" height="439" border="1" /></p>
      <p align="left">&nbsp;</p></td>
    </tr>
  </table>
</div>
<br></td>

    </div> <!-- /content-->
    <div id="widgets" class="columns">
<div id="fb-root"></div>
<script>(function (d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id))
            return;
        js = d.createElement(s);
        js.id = id;
        js.src = "//connect.facebook.net/nl_BE/sdk.js#xfbml=1&version=v2.6&appId=513131492093790";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>

<div class="fb-page mt1 mb1" data-href="https://www.facebook.com/seniorennet" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><div class="fb-xfbml-parse-ignore"><blockquote cite="https://www.facebook.com/seniorennet"><a href="https://www.facebook.com/seniorennet">Seniorennet</a></blockquote></div></div>


      <div id="printpage" class="widget-item">
        <div id="printpage-inner" class="padding-inner">
          <table width="100%">
            <tr>
              <td valign="middle"><a href="#" onClick="
              var str = document.URL;
              var url_check = str.indexOf( '?' );
              if ( url_check != -1 ) {
                 var urlke = window.location + '&afdrukken=ja';
              }
              else
              {
                var urlke = window.location + '?afdrukken=ja';
              }
              newwindow=window.open(urlke,'Afdrukken','height=800,width=700,menubar=no,scrollbars=yes,resizable=yes,status=no,toolbar=no,menubar=no');
              if (window.focus) {newwindow.focus()} return false;">Print pagina</a></td>
              <td valign="middle" align="right" width="20%"><img src="//www.seniorennet.be/images/icons/print.gif" alt="Pagina afdrukken" /></td>
            </tr>
          </table>
        </div>
      </div>
      <div class="widget-item">
        <div class="right-promobox">
          <div id='div-gpt-ad-1462356799442-3'>
            <script type='text/javascript'>
            googletag.cmd.push(function() { googletag.display('div-gpt-ad-1462356799442-3'); });
            </script>
          </div>
          <div id='div-gpt-ad-1462356799442-4'>
            <script type='text/javascript'>
            googletag.cmd.push(function() { googletag.display('div-gpt-ad-1462356799442-4'); });
            </script>
          </div>
          <div style="margin: 1em 0;" id='div-gpt-ad-1462356799442-1'>
            <script type='text/javascript'>
            googletag.cmd.push(function() { googletag.display('div-gpt-ad-1462356799442-1'); });
            </script>
          </div>
          <div id='div-gpt-ad-1462356799442-2'>
            <script type='text/javascript'>
            googletag.cmd.push(function() { googletag.display('div-gpt-ad-1462356799442-2'); });
            </script>
          </div>
        </div>
      </div>

      
      
      <div id="poll-wrap">
          
      </div>
      <script type="text/javascript">$('#poll-wrap').load('//www.seniorennet.be/polls/ajax-view');</script>

    </div>

<div id="pagefooter" class="columns">
  <div id="bookmarks">
    <div id="bookmarks-inner">
      <img src="https://www.facebook.com/favicon.ico" alt="facebook" />
      <a href="#" onclick="window.open('https://www.facebook.com/sharer/sharer.php?u='+encodeURIComponent(location.href),'facebook-share-dialog','width=626,height=436');return false;">
        Deel op Facebook
      </a>
    </div>
  </div>

  <a href="https://www.seniorennet.be/pages/redactie#abonneren">
    <div class="nb-inschrijven-footer">
      <form name="input" action="https://www.seniorennet.be/pages/redactie#abonneren" method="post">
      	<input name="state" type="hidden" id="state" value="I">
      	<input name="fv" type="hidden" id="hdfv" value="1">
    	<input name="ruri" type="hidden" id="hdruri" value="/help_tips_vragen_statistieken.php">
        <input type="email" name="emailadres" class="input" placeholder="emailadres" onClick="window.open('https://www.seniorennet.be/pages/redactie#abonneren');">
        <input type="submit" value="" class="button">
      </form>
    </div>
  </a>


  <div id="support">
  	<div id="support-inner">
    	Een vraag of een probleem op SeniorenNet? <br />
  		Kijk dan in het <a href="http://www.seniorennet.be/Pages/Overige/support.php">Helpcentrum (klik hier)</a>.
      Als je het daar niet kan vinden, helpen we je via e-mail op <a href="mailto:support@seniorennet.be">support@seniorennet.be</a>.
  	</div>
  </div>

</div>
</div>     
    

  <div id="footer_bottom"> Copyright &copy; 2001-2021 SeniorenNet - Alle rechten voorbehouden - 
  <a href="https://www.seniorennet.be/Pages/Overige/advert.php">Adverteren</a> | 
  <a href="https://www.seniorennet.be/Pages/Overige/support.php"  rel="nofollow">Contacteer ons</a> | 
  <a href="https://www.seniorennet.be/page/info/logo">Logo SeniorenNet</a> | 
  <a href="https://www.seniorennet.be/Pages/Overige/partners.php">Partners</a> | 
  <a href="https://www.seniorennet.be/Pages/Overige/over_ons.php">Over ons</a> | 
  <a href="https://www.seniorennet.be/Pages/Overige/privacy.php"  rel="nofollow">Privacy</a> | 
  <a href="https://www.seniorennet.be/page/info/disclaimer"  rel="nofollow">Disclaimer</a>

    </div>
  <div class="clear"></div>
</div>

<div class="clear"></div>


<script src="https://www.seniorennet.be/cache_js/foundation.js?v2018"></script>
<script type="text/javascript">
$(function(){
  $(document).foundation();
  $(window).scroll(function() {
    if ($(this).scrollTop() > 56){  
      $('#main-menubar').addClass("is-stuck");
      $('#main-menubar').removeClass("is-at-top");
      $("#main-menubar").css({
        'width': ($("#user-menubar").width() + 'px')
      });
      $("#user-menubar").css({
        'margin-bottom': ($("#main-menubar").height() + 'px')
      });
    } else{
      $('#main-menubar').addClass("is-at-top");
      $('#main-menubar').removeClass("is-stuck");
      $("#user-menubar").css({
        'margin-bottom': '0px'
      });
    }
  });
});
</script>

<!--[if lte IE 9]>
<script>
    var $buoop = {c:2,l:'nl'};
    function $buo_f(){
     var e = document.createElement("script");
     e.src = "//browser-update.org/update.min.js";
     document.body.appendChild(e);
    };
    try {document.addEventListener("DOMContentLoaded", $buo_f,false)}
    catch(e){window.attachEvent("onload", $buo_f)}


    $('.has-submenu').hover(function () {
        var left = $(this).offset().left - 245;
        $(this).find('.submenu').addClass('vertical').addClass('js-dropdown-active').css('left', left);
    }, function () {
        $(this).find('.submenu').removeClass('js-dropdown-active');
    });
</script>
<![endif]-->


<!--[if lt IE 9]>
    <script src="/js/ie8.js"></script>
<![endif]-->
</body>
</html>
</body>
</html>
