function URLEncode(plaintext){
	// The Javascript escape and unescape functions do not correspond
	// with what browsers actually do...
	var SAFECHARS = "0123456789" +					// Numeric
					"ABCDEFGHIJKLMNOPQRSTUVWXYZ" +	// Alphabetic Capital
					"abcdefghijklmnopqrstuvwxyz" +  //Alphabetic
					"-_";// RFC2396 Mark characters // We don't allow the .!~*'()
	var HEX = "0123456789ABCDEF";
  plaintext = plaintext.replace(/[\u00E0\u00E1\u00E2\u00E3\u00E4\u00E5]/g,'a'); 
  plaintext = plaintext.replace(/[\u00E7]/g,'c'); 
  plaintext = plaintext.replace(/[\u00E8\u00E9\u00EA\u00EB]/g,'e'); 
  plaintext = plaintext.replace(/[\u00EC\u00ED\u00EE\u00EF]/g,'i'); 
  plaintext = plaintext.replace(/[\u00F2\u00F3\u00F4\u00F5\u00F6\u00F8]/g,'o'); 
  plaintext = plaintext.replace(/[\u00F9\u00FA\u00FB\u00FC]/g,'u'); 
  plaintext = plaintext.replace(/[\u00FD\u00FF]/g,'y'); 
  plaintext = plaintext.replace(/[\u00F1]/g,'n'); 
  plaintext = plaintext.replace(/[\u0153]/g,'oe'); 
  plaintext = plaintext.replace(/[\u00E6]/g,'ae'); 
  plaintext = plaintext.replace(/[\u00DF]/g,'ss'); 
	var encoded = "";
	for (var i = 0; i < plaintext.length; i++ ) {
		var ch = plaintext.charAt(i);
	    if (ch == " ") {
		    encoded += "_";				// Replace the space by _
		} else if (SAFECHARS.indexOf(ch) != -1) {
		    encoded += ch;
		} else {
		  var charCode = ch.charCodeAt(0);
			if (charCode > 255) {
			    /*alert( "Unicode Character '" 
                        + ch 
                        + "' cannot be encoded using standard URL encoding.\n" +
				          "(URL encoding only supports 8-bit characters.)\n" +
						  "A space (+) will be substituted." );*/
				encoded += "";
			} else {
			  /*encoded += ch;
			  if(charcode)
				encoded += "%";
				encoded += HEX.charAt((charCode >> 4) & 0xF);
				encoded += HEX.charAt(charCode & 0xF);*/
				encoded += "";
			}
		}
	} // for
 return encoded;
};
