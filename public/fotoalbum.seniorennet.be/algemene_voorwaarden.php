<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-15" />
    <meta http-equiv="content-language" content="nl" />
            <meta name="Author" content="Seniorennet" />
    <meta http-equiv="Expires" content="Fri, 22 Jan 2021 17:15:18 CET" />
    <meta http-equiv="Pragma" content="no-cache" />
    <meta http-equiv="Cache-Control" content="no-cache" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    
    <title>SeniorenNet - Voor mensen met levenservaring en levenswijsheid</title>
    
          <!-- CSS NIEUWE LAYOUT -->


    <link rel="stylesheet" href="//www.seniorennet.be/css/print.css" type="text/css" media="print" />
    <link type="text/css" rel="stylesheet" href="//www.seniorennet.be/css/style.css?v20170717" />
    <link type="text/css" rel="stylesheet" href="//www.seniorennet.be/css/interactief.css?v=2" />
    <link type="text/css" rel="stylesheet" href="//www.seniorennet.be/css/fotoalbum.css" />
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
<link type="text/css" rel="stylesheet" href="https://www.seniorennet.be/css/sennet.css?v7" />


    <script src="https://www.google.com/jsapi?key=ABQIAAAA3sU-PhfOZtZRP_7JAqF6QRTBfbnnDyDqgyIAPZ5-G9HwiDjFoBTLf_CQBpr7R0r-7YUMhhY23aF0xQ" type="text/javascript"></script>

    <!--[if lt IE 9]>
        <link type="text/css" rel="stylesheet" href="https://www.seniorennet.be/cache_css/ie8.css?v1" />
            <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
            <script type="text/javascript" src="https://www.seniorennet.be/cache_js/ie8-head.js"></script>
    <![endif]-->
    <!--[if gt IE 8]><!-->
          <script src="//ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
        <!--<![endif]-->
    <script type="text/javascript" src="https://www.seniorennet.be/js/sennet.js"></script>

    
    
    

     
    

<script async='async' src='https://www.googletagservices.com/tag/js/gpt.js'></script>
<script>
    var googletag = googletag || {};
    googletag.cmd = googletag.cmd || [];
</script>

<script type='text/javascript'>
  googletag.cmd.push(function() {
    googletag.defineSlot('/8188103/SENNET_ROS_CONTENT1', [[300, 600], [300, 250]], 'div-gpt-ad-1462356799442-1').addService(googletag.pubads()).setCollapseEmptyDiv(true);
    googletag.defineSlot('/8188103/SENNET_ROS_CONTENT2', [[300, 600], [300, 100], [300, 250]], 'div-gpt-ad-1462356799442-2').addService(googletag.pubads()).setCollapseEmptyDiv(true);
    googletag.defineSlot('/8188103/SENNET_ROS_TOP', [[995, 125], [995, 123], [728, 90], [970, 90], [970, 250]], 'div-gpt-ad-1462356799442-0').addService(googletag.pubads()).setCollapseEmptyDiv(true);
    googletag.defineSlot('/8188103/SN_BE_Fotoalbum_Logo', '[150x125]', 'div-gpt-ad-1462356799442-3').addService(googletag.pubads()).setCollapseEmptyDiv(true);
    googletag.defineSlot('/8188103/SN_BE_Fotoalbum_Rectangle_Partner', '[150x125]', 'div-gpt-ad-1462356799442-4').addService(googletag.pubads()).setCollapseEmptyDiv(true);
    googletag.pubads().setTargeting('rubriek', ['old']);
    googletag.pubads().enableSingleRequest();
    googletag.enableServices();
  });

  function GA_googleFillSlot()
  {
    console.log('oldGAMTag');
  }
</script>
    
        
    </head>
<body  >
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-1117128-1', 'auto');
  ga('require', 'linkid', 'linkid.js');
  ga('require', 'displayfeatures');
  ga('send', 'pageview');

  var trackOutboundLink = function(url) {
    ga('send', 'event', 'outbound', 'click', url, {'hitCallback':
      function () {document.location = url;}
    });
  }

</script>

<script defer src="https://use.fontawesome.com/releases/v5.0.6/js/all.js"></script>

<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/nl_NL/all.js#xfbml=1";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>


   <!-- NIEUWE HEADER -->
<div id="page" class="row main-container">
  <div class="columns xlarge-12 small-12">
    <div id="header" class="row">
      <div class="user-menu row show-for-large" id="user-menubar">

        <div class="columns medium-3  menu-text main-logo">
            <a href="https://www.seniorennet.be/"><img src="https://www.seniorennet.be/img/layout/logo-seniorennet.svg?1516978662" alt="seniorennet" title="seniorennet" class="sn-logo hide-for-small-only" onerror="this.src=&#039;http:\/\/www.seniorennet.be/img/layout/logo-seniorennet.png&#039;"/></a>    
        </div>

        <div class="columns medium-5 xlarge-6">
            <form id="searchbox_003717546627450800428:obdjujbywam" action="https://www.seniorennet.be/zoeken/">
                <input value="003717546627450800428:obdjujbywam" name="cx" type="hidden"/>
                <input value="FORID:11" name="cof" type="hidden"/>
                <div class="search-button input-group">
                    <input id="q" name="q" type="search" class="input-group-field" placeholder="Zoeken">
                    <div class="input-group-button">
                        <button type="submit" name="sa" class="button"><i class="fas fa-search" data-fa-transform="left-6"></i></button>
                        <input type="submit" value="" />
                    </div>
                </div>
            </form>
        </div>
        <div class="columns medium-3 text-right" style="margin-left: -10px;">
            <ul>
                          <li><a class="" href="https://www.seniorennet.be/login"> <i class="hide-for-large fas fa-user"></i>Aanmelden</a></li>
              <li><a class="red" href="https://www.seniorennet.be/register">Registreren</a></li>
                          </ul>
        </div>
      </div>




      <div class="top-bar is-at-top" id="main-menubar">

        <div class="top-bar-left">
            <ul class="horizontal dropdown menu" data-dropdown-menu>
                <li id="sub-logo">
                    <a href="/"><img src="https://www.seniorennet.be/img/layout/logo-seniorennet.svg?1516978662" alt="seniorennet" title="seniorennet" onerror="this.src=&#039;https:\/\/www.seniorennet.be/img/layout/logo-seniorennet.png&#039;"/></a>
                </li>
                <li class="has-submenu">
                    <a href="https://www.seniorennet.be/nieuws">Nieuws</a>
                </li>
                <li class="has-submenu">
                    <a href="https://www.seniorennet.be/rubriek/gezondheid">Gezondheid</a>

                </li>
                <li class="has-submenu">
                    <a href="https://www.seniorennet.be/rubriek/wonen">Wonen</a>

                </li>
                <li class="has-submenu">
                    <a href="https://www.seniorennet.be/rubriek/geld-en-werk">Geld &amp; Recht</a>

                </li>
                <li class="has-submenu">
                    <a href="https://www.seniorennet.be/rubriek/toerisme">Toerisme</a>

                </li>
                <li class="has-submenu">
                    <a href="#">Vrije Tijd</a>

                    <ul class="submenu menu is-dropdown-submenu double" data-submenu>
                        <li><a href="https://www.seniorennet.be/Pages/grappig_schattig/">Grappig of Schattig</a></li>
                        <li><a href="/page/info/agenda">Activiteiten</a></li>
                        <li><a href="http://administratie.seniorennet.be/Nieuwsbrief/index_sennet_plezier.php">Dagelijks Grapje</a></li>
                        <li><a href="https://www.seniorennet.be/Pages/Vrije_tijd/spelletjes.php">Spelletjes</a></li>
                        <li><a href="https://www.seniorennet.be/Pages/Vrije_tijd/vrije_tijd.php">Vrije Tijd</a></li>
                        <li><a href="https://www.seniorennet.be/Pages/Vrije_tijd/Quiz/quiz.php">Quiz</a></li>
                        <li><a href="https://www.seniorennet.be/Pages/Visitekaartjes/visitekaartjes.php">Visitekaartjes</a></li>
                        <li><a href="https://www.seniorennet.be/ecards/">E-cards</a></li>
                        <li><a href="http://fotografie.seniorennet.be/">Fotografie</a></li>
                        <li><a href="https://www.seniorennet.be/rubriek/culinair/">Culinair</a></li>
                        <li><a href="https://www.seniorennet.be/rubriek/tuinkriebels/">Tuinkriebels</a></li>
                    </ul>
                </li>
                <li class="has-submenu">
                    <a href="#">Community</a>

                    <ul class="submenu menu is-dropdown-submenu " data-submenu>
                        <li><a href="https://www.seniorennet.be/pages/redactie">Nieuwsbrief</a></li>
                        <li><a href="https://www.seniorennet.be/forum/">Forum</a></li>
                        <li><a href="http://blog.seniorennet.be/">Blogs</a></li>
                        <li><a href="https://www.seniorennet.be/Pages/Vrije_tijd/chatbox.php">Chatbox</a></li>
                        <li><a href="https://www.seniorennet.be/Pages/Mailgroepen/mailgroepen.php">Mailgroepen</a></li>
                        <li><a href="http://fotoalbum.seniorennet.be/">Foto-album</a></li>
                        <li><a href="https://www.seniorennet.be/Pages/Zoeken/zoeken.php">Zoekertjes</a></li>
                        <li><a href="http://webmail.seniorennet.be/index.php">Webmail</a></li>
                    </ul>
                </li>
                <li class="has-submenu">
                    <a href="#">Nuttig</a>

                    <ul class="submenu menu is-dropdown-submenu " data-submenu>
                        <li><a href="https://www.seniorennet.be/pages/nuttige-links">Nuttige Info</a></li>
                        <li><a href="https://www.seniorennet.be/Pages/tips/tips.php">Tips</a></li>
                        <li><a href="https://www.seniorennet.be/Pages/Weer/weer.php">Weer</a></li>
                        <li><a href="https://www.seniorennet.be/Pages/Weer/buienradar.php">Buienradar</a></li>
                        <li><a href="https://www.seniorennet.be/Pages/Auto/verkeersinfo.php">Verkeersinfo</a></li>
                        <li><a href="https://www.seniorennet.be/rubriek/thuis-op-internet/">Thuis op Internet</a></li>
                        <li><a href="https://www.seniorennet.be/Pages/Thuis_op_internet/computerhulp.php">Computerhulp</a></li>
                        <li><a href="https://www.seniorennet.be/Pages/Vrije_tijd/omrekenen.php">Omrekenen</a></li>
                    </ul>
                </li>




            </ul>
        </div>

        <div class="top-bar-right">
            <ul class="dropdown menu" data-dropdown-menu id="menu-topicon" >
                <li>
                    <a href="/zoeken"><i class="fas fa-search fa-lg" data-fa-transform="right-1"></i></a>
                </li>
                <li>
                    <a href="#"><i class="fas fa-user-circle fa-lg" data-fa-transform="right-1"></i></a>
                    <ul class="submenu menu is-dropdown-submenu" data-submenu>
                        <li><a class="" href="https://www.seniorennet.be/login"> Aanmelden</a></li>
                        <li><a href="https://www.seniorennet.be/login">Registreren</a></li>
                    </ul>
                </li>
            </ul>
        </div>

    </div>



</div><!-- #header -->

    <div class="columns content">
      <!-- CONTENT -->




 <!-- EINDE NIEUWE HEADER -->





  </div>
  <div id="wrapper">
    <div id="wrapper-inner">

      <div id="advertisement" class="top">
          <div id="advertisement-inner">
            
            <div id='div-gpt-ad-1462356799442-0'>
            <script type='text/javascript'>
            googletag.cmd.push(function() { googletag.display('div-gpt-ad-1462356799442-0'); });
            </script>
            </div>  
            <div class="clear"></div>
          </div>
        </div>
      <div id="wrapper-inner-content">
      <div id="main" class="row">
    <div class="" id="content">

            <div id="waarvandaan">
        Waar vandaan: <a href="index.php">Foto albums</a> &gt; Algemene voorwaarden foto-albums
        </div>
        
        <h1 class="page-title kleur-interactief">Algemene voorwaarden foto-albums</h1>
                <div class="interactief-block">
                <h2>ALGEMEEN EN AANVAARDING</h2>
                <p>  Dit zijn de algemene gebruiksvoorwaarden van de SeniorenNet Foto-albums. Dit is een uitbreiding op de   bestaande <a href="http://www.seniorennet.be/Pages/Overige/disclaimer.php" target="_blank">disclaimer van SeniorenNet </a>en de bestaande <a href="http://www.seniorennet.be/Pages/Overige/privacy.php" target="_blank">privacy policy van SeniorenNet</a>. 
                    Indien u zich akkoord verklaart met deze gebruiksvoorwaarden, wordt verondersteld dat u ook akkoord bent met de <a href="http://www.seniorennet.be/Pages/Overige/disclaimer.php">disclaimer</a> en <a href="http://www.seniorennet.be/Pages/Overige/privacy.php">privacy policy</a> van SeniorenNet.<br />
                    Door het uitvoeren van de registratie en/of gebruik van de albums verklaart u zich   akkoord met deze voorwaarden en bent u onderworpen aan alle condities, rechten   en plichten die hierin vastgelegd zijn.</p>

                <h2>UW ALBUM</h2>
                <ul>
                        <li>Er zijn geen beperkingen op het aantal foto's per album</li>
                        <li>Er zijn geen beperkingen op het aantal albums per gebruiker</li>
                        <li>U kan dus zoveel foto's toevoegen als u zelf wenst</li>
                        <li>Uw foto's blijven voor onbeperkte periode staan in uw album</li>
                        <li>U kan uw album afschermen voor priv&eacute; gebruik, of enkel voor specifieke vrienden of familie openstellen, of voor iedereen toegankelijk maken</li>
                        <li>Bij vermoeden van misbruik van deze dienst door bijvoorbeeld (maar deze opsomming is niet limitatief) het toevoegen van extreem veel foto's met het doel om de SeniorenNet Fotoalbums (of de servers) te schaden of het aanmaken van extreem veel albums met het doel om de fotoalbums (of toplijsten) te schaden kan uw album/gebruikersnaam/foto's worden verwijderd en geblokkeerd.</li>
                </ul>
                
                <h2>GEDRAGSCODE</h2>
                <p>Als gebruiker van een SeniorenNet album verbindt u zich ertoe om:</p>
                <ul>
                      <li>Geen grafische elementen of teksten van SeniorenNet (banner, logo, header,   footer, copyright, advertentie &hellip;) te veranderen, te verwijderen, te verplaatsen   of onzichtbaar te maken. </li>
                      <li>Geen foto-album te cre&euml;ren, foto&rsquo;s, muziek, teksten, video&rsquo;s of andere inhoud te   publiceren, gebruiken, linken, hosten of uploaden:</li>
                    <ul>
                        <li>van beledigende, denigrerende, bedreigende, racistische, obscene, vulgaire, smadelijke, hatelijke, etnische, lasterlijke, onbetamelijke of onterende aard tegenover wie dan ook</li>
                        <li>dat ingaat tegen een Belgische, Nederlandse, Europese wet of land waar u woont, of dat inbreuk betekent op de rechten van derden in de meest algemene zin.</li>
                        <li>Dat kwaadaardige code bevat of zou kunnen bevatten zoals virussen, spyware keyloggers, illegale software, bestanden of gegevens van welke aard dan ook dat SeniorenNet of enig andere persoon nadeel kan berokkenen zoals vernietiging van gegevens, slechte functionaliteit, limitering van gebruiksmogelijkheden of welke aard dan ook.</li>
                        <li>Dat niet echt illegaal is maar die wel nadelig kan zijn of schade kan berokkenen van welke aard dan ook aan bepaalde personen, bedrijven, instanties, landen, regeringen of organisaties.</li>
                        <li>Dat van xenofobe, revisionistische, racistische aard is of dat aanzet tot deze zaken.</li>
                        <li>Dat schade kan berokkenen aan SeniorenNet of haar partners is de meest ruime zin, waaronder verstaan maar niet limitatief: het aanvallen van het informatica systeem, kopi&euml;ren van bestanden, hacken van het systeem, schade berokkenen aan de organisatie, informaticasysteem, databanken, leden, vrijwilligers, personeelsleden</li>
                        <li>Dat materiaal, producten of informatie verkoopt of doorverkoopt zonder voorafgaandelijke toestemming van SeniorenNet.be.</li>
                        <li>Dat pornografisch is van aard, inclusief maar niet limitatief tot kinderpornografie.</li>
                        <li>Dat aanzet tot het uitoefenen van illegale activiteiten, aanzet tot fysieke, psychologische of andere verwondingen ten opzichte van eender welke groep, individuen of dieren. Dit is inclusief maar niet limitatief tot bommen, granaten, wapens, dierenmishandeling.</li>
                        <li>Dat het verzamelen van gegevens is van andere gebruikers zonder hun toestemming of het publiceren van gegevens van derden zonder hun toestemming zoals, maar niet limitatief tot, geheime informatie, priv&eacute; informatie, zeer persoonlijke informatie, enz. dat mogelijk schade kan berokkenen van welke aard dan ook</li>
                    </ul>
                    <li>De intellectuele eigendomsrechten van anderen te respecteren, inclusief maar niet beperkend tot auteursrecht, copyright, trademark en patent.</li>
                    <li>In de meest algemene zin niets te doen dat in tegenspraak is met de goede zeden en de openbare orde, de geldende wetten en reglementeringen in onder andere maar niet limitatief het land waar de gebruiker woont en het land waar SeniorenNet gevestigd is, de lokale, nationale, internationale regels en wetten.</li>
                    <li>De SeniorenNet fotoalbum dienst niet door te verkopen of geld uit te slaan of andere voordelen te halen uit de geboden service onder welke vorm dan ook. </li>
                </ul>
                
                <h2>COPYRIGHT</h2>
                <ul>
                    <li>Foto's worden op de albums geplaatst zonder enige vorm van tussenkomst van SeniorenNet.</li>
                    <li>U blijft als gebruiker van het foto-album ten alle tijde verantwoordelijk voor de inhoud van uw foto-album. Dit op zowel de foto's, beeldbestanden, teksten, informatie, muziek, informatie... Het auteursrecht van het ingezonden werk berust bij de gebruiker en u draagt de eventuele gevolgen van een schending van het auteursrecht van derden. U ontneemt SeniorenNet elke verantwoordelijkheid voor schade die kan voortkomen uit de informatie die u publiceert en neemt zelf elke verantwoordelijkheid. Dit is inclusief de verantwoordelijkheid indien u toch gegevens publiceert die ingaan tegen (al dan niet gewild) een wet, regel, copyright, auteursrecht of van welke aard dan ook vermeld in deze gebruiksvoorwaarden.<br />
                      U staat zelf in voor de accuraatheid, juistheid, volledigheid en bruikbaarheid van de gegevens op uw foto-album.</li>
                    <li>Door het toevoegen van foto's op uw album geeft u (uiteraard) de toestemming aan SeniorenNet om die foto's op het internet te publiceren in uw foto-album. Uw album is beschikbaar via het adres dat u zelf heeft gekozen, in de vorm http://fotoalbum.seniorennet.be/[gebruikersnaam]. Voor extra gebruiksvriendelijkheid (en om typfouten van bezoekers van uw album op te vangen) kan het zijn dat uw album ook toegankelijk is via andere vergelijkbare adressen zoals http://album.seniorennet.be/[gebruikersnaam], http://fotoalbums.seniorennet.be/[gebruikersnaam], http://foto.seniorennet.be/[gebruikersnaam],... Deze adressen zijn slechts aliassen en doorstuuradressen naar uw normale albumadres.</li>
                    <li>SeniorenNet heeft het recht, maar niet de plicht, om materiaal waarvan ze vermoedt dat deze in strijd zijn van deze voorwaarden of van een lokale/nationale/internationale wet verwijderen. Dit zonder de gebruiker hiervan op de hoogte te stellen of toestemming te hoeven vragen.</li>     
                </ul>
                
                <h2>RECLAME MAKEN VOOR UW FOTOALBUM</h2>
                <p>Het is toegelaten reclame te maken voor uw eigen fotoalbums via eender welke methode of kanaal, zolang deze methode legaal is.<br />Uitzondering hierop is reclame maken voor uw fotoalbum door middel van "spam" via e-mail, op forums, gastenboeken,...; en kan verwijdering van uw fotoalbum tot gevolg hebben.</p>
                <ul>
                    <li>SeniorenNet heeft het recht om gegevens te wijzigen/verwijderen van een album of andere opgegeven informatie. Dit kan bijvoorbeeld zijn (maar dit is een niet limitatieve lijst): een album van categorie verplaatsen, een storende tekst verwijderen, titel aanpassen of verwijderen, EXIF informatie wissen, puntenscore wijzigen of recetten, een album/gebruiker uit de toplijsten halen,... Dit is alles zonder opgave van reden en de gebruiker dient hiervan niet verwittigd te worden.</li>
                    <li>SeniorenNet heeft het recht om een gebruiker te weigeren. SeniorenNet heeft tevens het recht om foto's, albums, teksten, muziek of eender welke andere vorm van informatie of content te verwijderen van haar diensten. Dit zonder de gebruiker hiervan te verwittigen of toestemming van te vragen. SeniorenNet hoeft hiervoor geen reden op te geven.</li>
                    <li>SeniorenNet heeft het recht, maar is niet verplicht, om aangifte te doen indien ze het vermoeden eheft dat er materiaal beschikbaar is gesteld dat strafbaar kan zijn. De gebruiker hoeft hiervoor niet op de hoogte worden gesteld en zonder verdere uitleg of opgave van reden.</li>
                    <li>SeniorenNet heeft het recht om eender welke foto, album, gebruiker of andere toegevoegde informatie te verwijderen. Dit zonder enige vorm van mogelijkheid tot schadevergoeding, zonder opgave van reden en zonder de gebruiker hiervan op de hoogte te stellen.</li>
                    <li>U bent zelf verantwoordelijk voor het bijhouden van een back-up van de inhoud en informatie van uw fotoalbum. </li>
                </ul>
                
                <h2>AANSPRAKELIJKHEID</h2>
                <p>SeniorenNet kan in geen enkel geval aansprakelijk worden gesteld voor beschadiging, verlies of verkeerde afbeelding van beeldbestanden en enige andere ingevoerde of geplaatste gegevens (zoals de exif-informatie, titels, zoekwoorden, teksten bij foto's, gastenboeken, punten bij foto's, waarderingen, reacties bij foto's,... ).
                SeniorenNet kan op geen enkele manier een schadevergoeding van welke aard dan ook aanvaarden voor welk verlies of beschadiging dan ook.</p>
                <p>SeniorenNet kan in geen enkel geval aansprakelijk worden gesteld voor het alsnog toegankelijk maken van foto's die normaal enkel priv&eacute; zouden moeten blijven of enkel voor specifieke vrienden of familie mogen getoond worden, voor het optreden van fouten in de software, scripts, programma's of diensten.</p>
                
                <h2>WEDSTRIJDEN</h2>
                <p>Het is mogelijk om mee te doen aan wedstrijden op de SeniorenNet Fotoalbums. Bij het insturen van een foto geeft u SeniorenNet de toestemming om de foto te publiceren in de rubriek Fotowedstrijden. Foto's die bij de eerste 3 eindigen bij een wedstrijd geven ook automatisch de toestemming om ook vermeld te worden bij een actueel artikel op SeniorenNet rond de wedstrijd of in andere media gebruikt te worden om te tonen wie de winnaars waren van die wedstrijd. Het is ook mogelijk, maar niet verplicht, dat de winnende foto's worden gepubliceerd in het magazine dat SeniorenNet uitgeeft, ook daar om te tonen wie de winnaars waren van de fotowedstrijd. Deze publicaties gebeuren zonder enige vorm van vergoeding die SeniorenNet u geeft voor het gebruik van deze foto's. De vermelding van alle ingestuurde foto's bij wedstrijden kan voor onbeperkte duur op de website en/of archieven gebeuren. SeniorenNet heeft het recht om bepaalde gebruikers of foto's te weigeren om mee te doen aan de wedstrijden. Dit voor een beperkte of onbeperkte periode en zonder opgave van reden.</p>
                
                <h2>MUZIEK / GELUIDEN / VIDEO </h2>
                <p>U mag muziek/geluiden/video plaatsen of gebruiken op uw fotoalbum, op voorwaarde dat u beschikt over de auteursrechten, of dat u specifieke toelating hebt van de auteur(s) of rechthebbenden van de auteursrechten.</p>
                
                <h2>WIJZIGINGEN</h2>
                <p>Deze gebruiksvoorwaarden kunnen wijzigen in de loop van de tijd. SeniorenNet heeft het recht om de dienst en/of gebruiksvoorwaarden van de fotoalbums te wijzigen zonder voorafgaande verwittiging en zonder dat u daardoor een schadevergoeding van welke aard dan ook kunt eisen met betrekking tot deze wijziging.<br />
                Indien u de dienst blijft gebruiken na deze wijziging, ook al is de wijziging niet meegedeeld, verklaart u zich akkoord met de wijziging. U dient deze voorwaarden regelmatig terug te lezen zodat u zelf de wijzigingen kan zien. SeniorenNet zal echter proberen de wijzigingen mee te delen via de website of e-mail, maar is niet verantwoordelijk indien dit niet altijd gebeurt zeker niet indien de wijziging van zeer kleine aard is.</p>
                
                <h2>PRIVACY</h2>
                <ul>
                    <li>SeniorenNet neemt de privacy van haar gebruikers uiterst serieus en volgt de nationale en internationale wetgevingen rond privacy. De door de gebruiker verstrekte informatie zal alleen gebruikt worden om de SeniorenNet Foto-albums en afgeleide diensten mogelijk te maken en te verbeteren. Zonder toestemming van de gebruiker worden gegevens niet aan derden ter beschikking gesteld. Een uitzondering hierop is indien van rechtswege gegevens worden gevorderd en de rechtsgang zou belemmerd worden indien SeniorenNet deze gegevens niet beschikbaar stelt.</li>

                    <li>Voor het goed functioneren van de website, foto-albums en afgeleide diensten zal SeniorenNet informatie verzamelen met IP-adresgegevens en cookies. Deze informatie wordt uitsluitend gebruikt voor het beter afstellen van de diensten, afstemmen op wensen van de gebruikers, problemen oplossen,... De service, gebruiksgemak en werking zal hierdoor voor de gebruiker verhoogd worden.</li>
                </ul>
                
                <h2>BE&Euml;INDIGING VAN DE DIENST</h2>
                <p>SeniorenNet heeft het recht deze dienst te be&euml;indigen zonder voorafgaande verwittiging met onmiddellijke ingang voor een beperkte of onbeperkte tijd en zonder dat u hiervoor een schadevergoeding van welke aard dan ook kunt eisen met betrekking tot deze be&euml;indiging.</p>
                <p>SeniorenNet heeft het recht deze dienst voor u te be&euml;indigen, te schrappen en/of ontoegankelijk te maken voor u en/of de bezoekers indien u zich niet aan deze gebruiksvoorwaarden houdt, indien SeniorenNet een vermoeden hiertoe heeft, indien u schade berokkent aan SeniorenNet en/of haar bezoekers/leden, zonder voorafgaande verwittiging en zonder dat u hiervoor een schadevergoeding van welke aard dan ook kunt eisen met betrekking tot deze be&euml;indiging.</p>
                <p>SeniorenNet heeft het recht om gebruikers te weigeren zonder opgave van reden, zonder voorafgaande verwittiging en zonder dat u hiervoor een schadevergoeding van welke aard dan ook kunt eisen met betrekking tot deze be&euml;indiging. Het gebruik van de SeniorenNet Fotoalbums is een privilege, geen recht. </p>
                <p>SeniorenNet heeft het recht informatie van uw fotoalbum te verwijderen, wijzigen of te weigeren zonder enige limieten, zonder voorafgaande verwittiging en zonder dat u hiervoor een schadevergoeding van welke aard dan ook kunt eisen met betrekking tot deze be&euml;indiging.</p>
                <p>SeniorenNet heeft het recht om de dienst fotoalbums volledig te be&euml;indigen in de toekomst voor alle gebruikers. U hebt dan geen recht op welke schadevergoeding dan ook. SeniorenNet zal echter wel proberen de totale be&euml;indiging op voorhand mee te delen zodat u de kans krijgt om alles te kopi&euml;ren en terug te bewaren. SeniorenNet is echter niet verantwoordelijk indien, om welke reden dan ook, u niet verwittigd werd van de be&euml;indiging.</p>
                
                <h2>PASWOORD, GEBRUIKERSNAAM, FOTOALBUMNAAM EN VEILIGHEID</h2>
                <p>Eenmaal dat u een gebruiker bent van de SeniorenNet Fotoalbums, heeft u een paswoord en een gebruikersnaam. U bent volledig verantwoordelijk voor de geheimhouding van de gegevens en indien iemand anders deze gegevens in uw plaats gebruikt worden ze geacht door uzelf ingegeven te zijn met alle verantwoordelijkheid vandien. U bent verder volledig verantwoordelijk voor alle activiteiten die u doet onder uw gebruikersnaam. U kan ten allen tijde uw paswoord of gebruikersnaam wijzigen. U heeft het recht om ten allen tijde te stoppen met uw fotoalbum. U blijft evenwel verantwoordelijk voor alle informatie die u er voordien heeft opgeplaatst en de rechten en plichten blijven gelden die in deze gebruiksvoorwaarden staan, ook al stopt u met het gebruik van de SeniorenNet Fotoalbums.<br />
                De fotoalbumnaam (url) die u kiest voor uw fotoalbum komt overeen met de url die u hiervoor krijgt. Onder normale omstandigheden is deze definitief voor uw fotoalbum. SeniorenNet is echter niet verantwoordelijk indien door een fout, faling, wijziging van welke aard dan ook deze naam verloren gaat en zelfs mogelijk wordt doorgegeven aan een andere gebruiker. U kan hiervoor geen schadevergoeding eisen van welke aard dan ook.</p>
                
                <h2>ALGEMEENHEDEN</h2>
                <p>Enkel het Belgische recht is van toepassing op deze voorwaarden. Elk geschil betreffende deze voorwaarden en/ of de fotoalbums dat niet in der minne kan worden geregeld, valt onder de bevoegdheid van de rechtbanken van Antwerpen, tenzij u uw akkoord geeft tot arbitrage op het moment van het onstaan van het geschil.</p>
                </div>
                

    </div> <!-- /content-->
    <div id="widgets" class="columns">
<div id="fb-root"></div>
<script>(function (d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id))
            return;
        js = d.createElement(s);
        js.id = id;
        js.src = "//connect.facebook.net/nl_BE/sdk.js#xfbml=1&version=v2.6&appId=513131492093790";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>

<div class="fb-page mt1 mb1" data-href="https://www.facebook.com/seniorennet" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><div class="fb-xfbml-parse-ignore"><blockquote cite="https://www.facebook.com/seniorennet"><a href="https://www.facebook.com/seniorennet">Seniorennet</a></blockquote></div></div>


      <div id="printpage" class="widget-item">
        <div id="printpage-inner" class="padding-inner">
          <table width="100%">
            <tr>
              <td valign="middle"><a href="#" onClick="
              var str = document.URL;
              var url_check = str.indexOf( '?' );
              if ( url_check != -1 ) {
                 var urlke = window.location + '&afdrukken=ja';
              }
              else
              {
                var urlke = window.location + '?afdrukken=ja';
              }
              newwindow=window.open(urlke,'Afdrukken','height=800,width=700,menubar=no,scrollbars=yes,resizable=yes,status=no,toolbar=no,menubar=no');
              if (window.focus) {newwindow.focus()} return false;">Print pagina</a></td>
              <td valign="middle" align="right" width="20%"><img src="//www.seniorennet.be/images/icons/print.gif" alt="Pagina afdrukken" /></td>
            </tr>
          </table>
        </div>
      </div>
      <div class="widget-item">
        <div class="right-promobox">
          <div id='div-gpt-ad-1462356799442-3'>
            <script type='text/javascript'>
            googletag.cmd.push(function() { googletag.display('div-gpt-ad-1462356799442-3'); });
            </script>
          </div>
          <div id='div-gpt-ad-1462356799442-4'>
            <script type='text/javascript'>
            googletag.cmd.push(function() { googletag.display('div-gpt-ad-1462356799442-4'); });
            </script>
          </div>
          <div style="margin: 1em 0;" id='div-gpt-ad-1462356799442-1'>
            <script type='text/javascript'>
            googletag.cmd.push(function() { googletag.display('div-gpt-ad-1462356799442-1'); });
            </script>
          </div>
          <div id='div-gpt-ad-1462356799442-2'>
            <script type='text/javascript'>
            googletag.cmd.push(function() { googletag.display('div-gpt-ad-1462356799442-2'); });
            </script>
          </div>
        </div>
      </div>

      
      
      <div id="poll-wrap">
          
      </div>
      <script type="text/javascript">$('#poll-wrap').load('//www.seniorennet.be/polls/ajax-view');</script>

    </div>

<div id="pagefooter" class="columns">
  <div id="bookmarks">
    <div id="bookmarks-inner">
      <img src="https://www.facebook.com/favicon.ico" alt="facebook" />
      <a href="#" onclick="window.open('https://www.facebook.com/sharer/sharer.php?u='+encodeURIComponent(location.href),'facebook-share-dialog','width=626,height=436');return false;">
        Deel op Facebook
      </a>
    </div>
  </div>

  <a href="https://www.seniorennet.be/pages/redactie#abonneren">
    <div class="nb-inschrijven-footer">
      <form name="input" action="https://www.seniorennet.be/pages/redactie#abonneren" method="post">
      	<input name="state" type="hidden" id="state" value="I">
      	<input name="fv" type="hidden" id="hdfv" value="1">
    	<input name="ruri" type="hidden" id="hdruri" value="/algemene_voorwaarden.php">
        <input type="email" name="emailadres" class="input" placeholder="emailadres" onClick="window.open('https://www.seniorennet.be/pages/redactie#abonneren');">
        <input type="submit" value="" class="button">
      </form>
    </div>
  </a>


  <div id="support">
  	<div id="support-inner">
    	Een vraag of een probleem op SeniorenNet? <br />
  		Kijk dan in het <a href="http://www.seniorennet.be/Pages/Overige/support.php">Helpcentrum (klik hier)</a>.
      Als je het daar niet kan vinden, helpen we je via e-mail op <a href="mailto:support@seniorennet.be">support@seniorennet.be</a>.
  	</div>
  </div>

</div>
</div>     
    

  <div id="footer_bottom"> Copyright &copy; 2001-2021 SeniorenNet - Alle rechten voorbehouden - 
  <a href="https://www.seniorennet.be/Pages/Overige/advert.php">Adverteren</a> | 
  <a href="https://www.seniorennet.be/Pages/Overige/support.php"  rel="nofollow">Contacteer ons</a> | 
  <a href="https://www.seniorennet.be/page/info/logo">Logo SeniorenNet</a> | 
  <a href="https://www.seniorennet.be/Pages/Overige/partners.php">Partners</a> | 
  <a href="https://www.seniorennet.be/Pages/Overige/over_ons.php">Over ons</a> | 
  <a href="https://www.seniorennet.be/Pages/Overige/privacy.php"  rel="nofollow">Privacy</a> | 
  <a href="https://www.seniorennet.be/page/info/disclaimer"  rel="nofollow">Disclaimer</a>

    </div>
  <div class="clear"></div>
</div>

<div class="clear"></div>


<script src="https://www.seniorennet.be/cache_js/foundation.js?v2018"></script>
<script type="text/javascript">
$(function(){
  $(document).foundation();
  $(window).scroll(function() {
    if ($(this).scrollTop() > 56){  
      $('#main-menubar').addClass("is-stuck");
      $('#main-menubar').removeClass("is-at-top");
      $("#main-menubar").css({
        'width': ($("#user-menubar").width() + 'px')
      });
      $("#user-menubar").css({
        'margin-bottom': ($("#main-menubar").height() + 'px')
      });
    } else{
      $('#main-menubar').addClass("is-at-top");
      $('#main-menubar').removeClass("is-stuck");
      $("#user-menubar").css({
        'margin-bottom': '0px'
      });
    }
  });
});
</script>

<!--[if lte IE 9]>
<script>
    var $buoop = {c:2,l:'nl'};
    function $buo_f(){
     var e = document.createElement("script");
     e.src = "//browser-update.org/update.min.js";
     document.body.appendChild(e);
    };
    try {document.addEventListener("DOMContentLoaded", $buo_f,false)}
    catch(e){window.attachEvent("onload", $buo_f)}


    $('.has-submenu').hover(function () {
        var left = $(this).offset().left - 245;
        $(this).find('.submenu').addClass('vertical').addClass('js-dropdown-active').css('left', left);
    }, function () {
        $(this).find('.submenu').removeClass('js-dropdown-active');
    });
</script>
<![endif]-->


<!--[if lt IE 9]>
    <script src="/js/ie8.js"></script>
<![endif]-->
</body>
</html>
