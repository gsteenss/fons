</html>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-15" />
    <meta http-equiv="content-language" content="nl" />
            <meta name="Author" content="Seniorennet" />
    <meta http-equiv="Expires" content="Fri, 22 Jan 2021 17:14:57 CET" />
    <meta http-equiv="Pragma" content="no-cache" />
    <meta http-equiv="Cache-Control" content="no-cache" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    
    <title>SeniorenNet - Voor mensen met levenservaring en levenswijsheid</title>
    
          <!-- CSS NIEUWE LAYOUT -->


    <link rel="stylesheet" href="//www.seniorennet.be/css/print.css" type="text/css" media="print" />
    <link type="text/css" rel="stylesheet" href="//www.seniorennet.be/css/style.css?v20170717" />
    <link type="text/css" rel="stylesheet" href="//www.seniorennet.be/css/interactief.css?v=2" />
    <link type="text/css" rel="stylesheet" href="//www.seniorennet.be/css/fotoalbum.css" />
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
<link type="text/css" rel="stylesheet" href="https://www.seniorennet.be/css/sennet.css?v7" />


    <script src="https://www.google.com/jsapi?key=ABQIAAAA3sU-PhfOZtZRP_7JAqF6QRTBfbnnDyDqgyIAPZ5-G9HwiDjFoBTLf_CQBpr7R0r-7YUMhhY23aF0xQ" type="text/javascript"></script>

    <!--[if lt IE 9]>
        <link type="text/css" rel="stylesheet" href="https://www.seniorennet.be/cache_css/ie8.css?v1" />
            <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
            <script type="text/javascript" src="https://www.seniorennet.be/cache_js/ie8-head.js"></script>
    <![endif]-->
    <!--[if gt IE 8]><!-->
          <script src="//ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
        <!--<![endif]-->
    <script type="text/javascript" src="https://www.seniorennet.be/js/sennet.js"></script>

    
    
    

     
    

<script async='async' src='https://www.googletagservices.com/tag/js/gpt.js'></script>
<script>
    var googletag = googletag || {};
    googletag.cmd = googletag.cmd || [];
</script>

<script type='text/javascript'>
  googletag.cmd.push(function() {
    googletag.defineSlot('/8188103/SENNET_ROS_CONTENT1', [[300, 600], [300, 250]], 'div-gpt-ad-1462356799442-1').addService(googletag.pubads()).setCollapseEmptyDiv(true);
    googletag.defineSlot('/8188103/SENNET_ROS_CONTENT2', [[300, 600], [300, 100], [300, 250]], 'div-gpt-ad-1462356799442-2').addService(googletag.pubads()).setCollapseEmptyDiv(true);
    googletag.defineSlot('/8188103/SENNET_ROS_TOP', [[995, 125], [995, 123], [728, 90], [970, 90], [970, 250]], 'div-gpt-ad-1462356799442-0').addService(googletag.pubads()).setCollapseEmptyDiv(true);
    googletag.defineSlot('/8188103/SN_BE_Fotoalbum_Logo', '[150x125]', 'div-gpt-ad-1462356799442-3').addService(googletag.pubads()).setCollapseEmptyDiv(true);
    googletag.defineSlot('/8188103/SN_BE_Fotoalbum_Rectangle_Partner', '[150x125]', 'div-gpt-ad-1462356799442-4').addService(googletag.pubads()).setCollapseEmptyDiv(true);
    googletag.pubads().setTargeting('rubriek', ['old']);
    googletag.pubads().enableSingleRequest();
    googletag.enableServices();
  });

  function GA_googleFillSlot()
  {
    console.log('oldGAMTag');
  }
</script>
    
        
    </head>
<body  >
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-1117128-1', 'auto');
  ga('require', 'linkid', 'linkid.js');
  ga('require', 'displayfeatures');
  ga('send', 'pageview');

  var trackOutboundLink = function(url) {
    ga('send', 'event', 'outbound', 'click', url, {'hitCallback':
      function () {document.location = url;}
    });
  }

</script>

<script defer src="https://use.fontawesome.com/releases/v5.0.6/js/all.js"></script>

<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/nl_NL/all.js#xfbml=1";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>


   <!-- NIEUWE HEADER -->
<div id="page" class="row main-container">
  <div class="columns xlarge-12 small-12">
    <div id="header" class="row">
      <div class="user-menu row show-for-large" id="user-menubar">

        <div class="columns medium-3  menu-text main-logo">
            <a href="https://www.seniorennet.be/"><img src="https://www.seniorennet.be/img/layout/logo-seniorennet.svg?1516978662" alt="seniorennet" title="seniorennet" class="sn-logo hide-for-small-only" onerror="this.src=&#039;http:\/\/www.seniorennet.be/img/layout/logo-seniorennet.png&#039;"/></a>    
        </div>

        <div class="columns medium-5 xlarge-6">
            <form id="searchbox_003717546627450800428:obdjujbywam" action="https://www.seniorennet.be/zoeken/">
                <input value="003717546627450800428:obdjujbywam" name="cx" type="hidden"/>
                <input value="FORID:11" name="cof" type="hidden"/>
                <div class="search-button input-group">
                    <input id="q" name="q" type="search" class="input-group-field" placeholder="Zoeken">
                    <div class="input-group-button">
                        <button type="submit" name="sa" class="button"><i class="fas fa-search" data-fa-transform="left-6"></i></button>
                        <input type="submit" value="" />
                    </div>
                </div>
            </form>
        </div>
        <div class="columns medium-3 text-right" style="margin-left: -10px;">
            <ul>
                          <li><a class="" href="https://www.seniorennet.be/login"> <i class="hide-for-large fas fa-user"></i>Aanmelden</a></li>
              <li><a class="red" href="https://www.seniorennet.be/register">Registreren</a></li>
                          </ul>
        </div>
      </div>




      <div class="top-bar is-at-top" id="main-menubar">

        <div class="top-bar-left">
            <ul class="horizontal dropdown menu" data-dropdown-menu>
                <li id="sub-logo">
                    <a href="/"><img src="https://www.seniorennet.be/img/layout/logo-seniorennet.svg?1516978662" alt="seniorennet" title="seniorennet" onerror="this.src=&#039;https:\/\/www.seniorennet.be/img/layout/logo-seniorennet.png&#039;"/></a>
                </li>
                <li class="has-submenu">
                    <a href="https://www.seniorennet.be/nieuws">Nieuws</a>
                </li>
                <li class="has-submenu">
                    <a href="https://www.seniorennet.be/rubriek/gezondheid">Gezondheid</a>

                </li>
                <li class="has-submenu">
                    <a href="https://www.seniorennet.be/rubriek/wonen">Wonen</a>

                </li>
                <li class="has-submenu">
                    <a href="https://www.seniorennet.be/rubriek/geld-en-werk">Geld &amp; Recht</a>

                </li>
                <li class="has-submenu">
                    <a href="https://www.seniorennet.be/rubriek/toerisme">Toerisme</a>

                </li>
                <li class="has-submenu">
                    <a href="#">Vrije Tijd</a>

                    <ul class="submenu menu is-dropdown-submenu double" data-submenu>
                        <li><a href="https://www.seniorennet.be/Pages/grappig_schattig/">Grappig of Schattig</a></li>
                        <li><a href="/page/info/agenda">Activiteiten</a></li>
                        <li><a href="http://administratie.seniorennet.be/Nieuwsbrief/index_sennet_plezier.php">Dagelijks Grapje</a></li>
                        <li><a href="https://www.seniorennet.be/Pages/Vrije_tijd/spelletjes.php">Spelletjes</a></li>
                        <li><a href="https://www.seniorennet.be/Pages/Vrije_tijd/vrije_tijd.php">Vrije Tijd</a></li>
                        <li><a href="https://www.seniorennet.be/Pages/Vrije_tijd/Quiz/quiz.php">Quiz</a></li>
                        <li><a href="https://www.seniorennet.be/Pages/Visitekaartjes/visitekaartjes.php">Visitekaartjes</a></li>
                        <li><a href="https://www.seniorennet.be/ecards/">E-cards</a></li>
                        <li><a href="http://fotografie.seniorennet.be/">Fotografie</a></li>
                        <li><a href="https://www.seniorennet.be/rubriek/culinair/">Culinair</a></li>
                        <li><a href="https://www.seniorennet.be/rubriek/tuinkriebels/">Tuinkriebels</a></li>
                    </ul>
                </li>
                <li class="has-submenu">
                    <a href="#">Community</a>

                    <ul class="submenu menu is-dropdown-submenu " data-submenu>
                        <li><a href="https://www.seniorennet.be/pages/redactie">Nieuwsbrief</a></li>
                        <li><a href="https://www.seniorennet.be/forum/">Forum</a></li>
                        <li><a href="http://blog.seniorennet.be/">Blogs</a></li>
                        <li><a href="https://www.seniorennet.be/Pages/Vrije_tijd/chatbox.php">Chatbox</a></li>
                        <li><a href="https://www.seniorennet.be/Pages/Mailgroepen/mailgroepen.php">Mailgroepen</a></li>
                        <li><a href="http://fotoalbum.seniorennet.be/">Foto-album</a></li>
                        <li><a href="https://www.seniorennet.be/Pages/Zoeken/zoeken.php">Zoekertjes</a></li>
                        <li><a href="http://webmail.seniorennet.be/index.php">Webmail</a></li>
                    </ul>
                </li>
                <li class="has-submenu">
                    <a href="#">Nuttig</a>

                    <ul class="submenu menu is-dropdown-submenu " data-submenu>
                        <li><a href="https://www.seniorennet.be/pages/nuttige-links">Nuttige Info</a></li>
                        <li><a href="https://www.seniorennet.be/Pages/tips/tips.php">Tips</a></li>
                        <li><a href="https://www.seniorennet.be/Pages/Weer/weer.php">Weer</a></li>
                        <li><a href="https://www.seniorennet.be/Pages/Weer/buienradar.php">Buienradar</a></li>
                        <li><a href="https://www.seniorennet.be/Pages/Auto/verkeersinfo.php">Verkeersinfo</a></li>
                        <li><a href="https://www.seniorennet.be/rubriek/thuis-op-internet/">Thuis op Internet</a></li>
                        <li><a href="https://www.seniorennet.be/Pages/Thuis_op_internet/computerhulp.php">Computerhulp</a></li>
                        <li><a href="https://www.seniorennet.be/Pages/Vrije_tijd/omrekenen.php">Omrekenen</a></li>
                    </ul>
                </li>




            </ul>
        </div>

        <div class="top-bar-right">
            <ul class="dropdown menu" data-dropdown-menu id="menu-topicon" >
                <li>
                    <a href="/zoeken"><i class="fas fa-search fa-lg" data-fa-transform="right-1"></i></a>
                </li>
                <li>
                    <a href="#"><i class="fas fa-user-circle fa-lg" data-fa-transform="right-1"></i></a>
                    <ul class="submenu menu is-dropdown-submenu" data-submenu>
                        <li><a class="" href="https://www.seniorennet.be/login"> Aanmelden</a></li>
                        <li><a href="https://www.seniorennet.be/login">Registreren</a></li>
                    </ul>
                </li>
            </ul>
        </div>

    </div>



</div><!-- #header -->

    <div class="columns content">
      <!-- CONTENT -->




 <!-- EINDE NIEUWE HEADER -->





  </div>
  <div id="wrapper">
    <div id="wrapper-inner">

      <div id="advertisement" class="top">
          <div id="advertisement-inner">
            
            <div id='div-gpt-ad-1462356799442-0'>
            <script type='text/javascript'>
            googletag.cmd.push(function() { googletag.display('div-gpt-ad-1462356799442-0'); });
            </script>
            </div>  
            <div class="clear"></div>
          </div>
        </div>
      <div id="wrapper-inner-content">
      <div id="main" class="row">
    <div class="" id="content">

    <style type="text/css">
<!--
p.MsoNormal {
margin-top:0cm;
margin-right:0cm;
margin-bottom:10.0pt;
margin-left:0cm;
}
-->
</style>
<div class="body"> 
  <table width="100%" border="0" cellspacing="5" cellpadding="0">
    <tr> 
      <td valign="top"> 
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr> 
            <td bgcolor="#BABA74" height="20"><font color="#003333"> Waar vandaan: <a href="index.php">Foto albums</a> &gt; <a href="help.php">Help</a> &gt; Overzicht belangrijkste functionaliteiten</font></td>
          </tr>
        </table>
        <h1 align="left">

Overzicht belangrijkste functionaliteiten</h1>
        <table width="100%" border="0">
          <tr>
            <td width="4%" height="23" align="center" valign="baseline"><img src="images/fotoalbum_mogelijkheden.jpg" alt="Fotoalbum functie" width="20" height="19" /> </td>
            <td width="96%">Volledig Nederlandstalig</td>
          </tr>
          <tr>
            <td height="23" align="center" valign="baseline"><img src="images/fotoalbum_mogelijkheden.jpg" alt="Fotoalbum functie" width="20" height="19" /> </td>
            <td>De fotoalbums zijn gratis. Er zijn geen verborgen kosten. Het is ook niet mogelijk om bij te betalen voor extra diensten, aangezien alle diensten gratis zijn.</td>
          </tr>
          <tr>
            <td height="23" align="center" valign="baseline"><img src="images/fotoalbum_mogelijkheden.jpg" alt="Fotoalbum functie" width="20" height="19" /> </td>
            <td>Je eigen foto's op het internet plaatsen</td>
          </tr>
          <tr>
            <td height="23" align="center" valign="baseline"><img src="images/fotoalbum_mogelijkheden.jpg" alt="Fotoalbum functie" width="20" height="19" /> </td>
            <td>Er zijn GEEN limieten. Je kan zoveel foto's plaatsen op het internet als je zelf wil. Of dit er nu 1, 10 of 10.000 zijn, dat maakt niet uit. Ook de bestandsgrootte speelt geen rol. Of je foto nu maar enkele kb groot is, of 30 MB per foto maakt niet uit. Ook de afmetingen van de foto's maken niet uit. Het is evengoed mogelijk een mini foto te uploaden als een foto ter grootte van een poster. Er zijn ook geen limieten op dataverkeer dat uw fotoalbum genereert of het aantal bezoekers dat het ontvangt.</td>
          </tr>
          <tr>
            <td height="23" align="center" valign="baseline"><img src="images/fotoalbum_mogelijkheden.jpg" alt="Fotoalbum functie" width="20" height="19" /></td>
            <td>Geen beperking van tijd dat uw foto's online blijven staan. </td>
          </tr>
          <tr>
            <td height="23" align="center" valign="baseline"><img src="images/fotoalbum_mogelijkheden.jpg" alt="Fotoalbum functie" width="20" height="19" /> </td>
            <td>E&eacute;n gebruiker kan zoveel albums maken als gewenst. Er is geen limiet op het aantal albums per gebruiker. Er is geen limiet op het aantal foto's per album.</td>
          </tr>
          <tr>
            <td height="23" align="center" valign="baseline"><img src="images/fotoalbum_mogelijkheden.jpg" alt="Fotoalbum functie" width="20" height="19" /> </td>
            <td>Ondersteuning van JPEG, GIF, animated GIF, PNG, TIFF en BMP</td>
          </tr>
          <tr>
            <td height="23" align="center" valign="baseline"><img src="images/fotoalbum_mogelijkheden.jpg" alt="Fotoalbum functie" width="20" height="19" /> </td>
            <td>Zeer gebruiksvriendelijk. Wij starten bij de ontwikkeling van de gebruiker uit, en starten dan pas met de programmatuur. Heel anders dan de meeste andere software en websites. Gebruiksvriendelijkheid is d&eacute; rode draad bij de ontwikkeling van de fotoalbums.</td>
          </tr>
          <tr>
            <td height="23" align="center" valign="baseline"><img src="images/fotoalbum_mogelijkheden.jpg" alt="Fotoalbum functie" width="20" height="19" /> </td>
            <td>Registeren in minder dan 2 minuten. U hoeft slechts 6 kleine vragen in te vullen om van start te gaan. Ook registreren is zonder kosten.</td>
          </tr>
          <tr>
            <td height="23" align="center" valign="baseline"><img src="images/fotoalbum_mogelijkheden.jpg" alt="Fotoalbum functie" width="20" height="19" /> </td>
            <td>Je hebt geen technische kennis nodig om met de fotoalbums aan de slag te gaan. </td>
          </tr>
          <tr>
            <td height="23" align="center" valign="baseline"><img src="images/fotoalbum_mogelijkheden.jpg" alt="Fotoalbum functie" width="20" height="19" /> </td>
            <td>Voor wie het toch allemaal niet helemaal duidelijk is, zijn er zeer uitgebreide helpbestanden. Inclusief stap-voor-stap handleidingen geschreven door de bijzonder succesvolle auteur van de boeken &quot;internet na 50&quot;, &quot;veilig surfen na 50&quot; en &quot;computeren na 50&quot;.</td>
          </tr>
          <tr>
            <td height="23" align="center" valign="baseline"><img src="images/fotoalbum_mogelijkheden.jpg" alt="Fotoalbum functie" width="20" height="19" /> </td>
            <td>Mogelijk foto's strikt priv&eacute; te houden. Mogelijkheid om foto's open te stellen aan zeer specifieke vrienden of familie. Enkel wie jij specifiek toegang geeft kan ze zien; de rest van de wereld niet.</td>
          </tr>
          <tr>
            <td height="23" align="center" valign="baseline"><img src="images/fotoalbum_mogelijkheden.jpg" alt="Fotoalbum functie" width="20" height="19" /> </td>
            <td>Mogelijk om foto's open te stellen voor iedereen. Hierdoor kunnen uw foto's gevonden worden door eender welke bezoeker en kunnen zij ook genieten van de prachtige foto's die je hebt gemaakt.</td>
          </tr>
          <tr>
            <td height="23" align="center" valign="baseline"><img src="images/fotoalbum_mogelijkheden.jpg" alt="Fotoalbum functie" width="20" height="19" /> </td>
            <td>Er is een blog (website) waarop het laatste nieuws van de foto-albums wordt gebracht, zodat je steeds op de hoogte bent.</td>
          </tr>
          <tr>
            <td height="23" align="center" valign="baseline"><img src="images/fotoalbum_mogelijkheden.jpg" alt="Fotoalbum functie" width="20" height="19" /> </td>
            <td>24/24u, 7 dagen per week en 366 dagen per jaar monitoring van de foto-albums. Er is permanent een team dat klaarstaat om in te grijpen bij eventuele problemen en snel technische problemen op te lossen zodat er maximale betrouwbaarheid, maximale bereikbaarheid en werking is voor de fotoalbums.</td>
          </tr>
          <tr>
            <td height="23" align="center" valign="baseline"><img src="images/fotoalbum_mogelijkheden.jpg" alt="Fotoalbum functie" width="20" height="19" /> </td>
            <td>Toplijsten van de meest succesvolle foto-albums, recentste foto's, meest gewaardeerde foto's, recentste gebruikers,...</td>
          </tr>
          <tr>
            <td height="23" align="center" valign="baseline"><img src="images/fotoalbum_mogelijkheden.jpg" alt="Fotoalbum functie" width="20" height="19" /> </td>
            <td>Meedoen aan fotowedstrijden</td>
          </tr>
          <tr>
            <td height="23" align="center" valign="baseline"><img src="images/fotoalbum_mogelijkheden.jpg" alt="Fotoalbum functie" width="20" height="19" /> </td>
            <td>Indelen van album in categorie&euml;n</td>
          </tr>
          <tr>
            <td height="23" align="center" valign="baseline"><img src="images/fotoalbum_mogelijkheden.jpg" alt="Fotoalbum functie" width="20" height="19" /> </td>
            <td>Ondersteuning van de meest courante browsers en besturingssystemen. Dit is onder meer, maar daarom niet beperkt tot: Windows 95, 98, 2000, Me, XP en Windows Vista. Ook Mac, Apple en Linux zijn ondersteund. Browsers waarop de fotoalbums getest worden en dus zeker zouden op moeten werken: Internet Explorer 6, Internet Explorer 7, Internet Explorer 8, Firefox 2, Firefox 3, Google Chrome, Safari, Opera.</td>
          </tr>
          <tr>
            <td height="23" align="center" valign="baseline"><img src="images/fotoalbum_mogelijkheden.jpg" alt="Fotoalbum functie" width="20" height="19" /> </td>
            <td>Mogelijkheid bij vergeten paswoord om het opnieuw op te vragen</td>
          </tr>
          <tr>
            <td height="23" align="center" valign="baseline"><img src="images/fotoalbum_mogelijkheden.jpg" alt="Fotoalbum functie" width="20" height="19" /> </td>
            <td>Toevoegen van foto's kan zeer eenvoudig en snel via speciaal webprogramma. Gewoon foto's selecteren van je harde schijf en toevoegen.</td>
          </tr>
          <tr>
            <td height="23" align="center" valign="baseline"><img src="images/fotoalbum_mogelijkheden.jpg" alt="Fotoalbum functie" width="20" height="19" /> </td>
            <td>Mogelijkheid om foto per foto toe te voegen.</td>
          </tr>
          <tr>
            <td height="23" align="center" valign="baseline"><img src="images/fotoalbum_mogelijkheden.jpg" alt="Fotoalbum functie" width="20" height="19" /> </td>
            <td>Mogelijk om geluid bij een foto te plaatsen</td>
          </tr>
          <tr>
            <td height="23" align="center" valign="baseline"><img src="images/fotoalbum_mogelijkheden.jpg" alt="Fotoalbum functie" width="20" height="19" /> </td>
            <td>Mogelijk om een titel te plaatsen bij een foto</td>
          </tr>
          <tr>
            <td height="23" align="center" valign="baseline"><img src="images/fotoalbum_mogelijkheden.jpg" alt="Fotoalbum functie" width="20" height="19" /> </td>
            <td>Mogelijk om commentaar te plaatsen bij een foto. Je kan een onbeperkte lengte plaatsen aan tekst. </td>
          </tr>
          <tr>
            <td height="23" align="center" valign="baseline"><img src="images/fotoalbum_mogelijkheden.jpg" alt="Fotoalbum functie" width="20" height="19" /> </td>
            <td>Ook mogelijk om HTML te plaatsen bij een foto om extra functies of effecten toe te voegen</td>
          </tr>
          <tr>
            <td height="23" align="center" valign="baseline"><img src="images/fotoalbum_mogelijkheden.jpg" alt="Fotoalbum functie" width="20" height="19" /> </td>
            <td>Verschillende uitzichten/weergaves van het fotoalbum: miniatuur, micro, gallerij, gallerij + tekst; meestal al dan niet in een beperkt aantal foto's per pagina of alle foto's op &eacute;&eacute;n pagina.</td>
          </tr>
         
          <tr>
            <td width="4%" height="23" align="center" valign="baseline"><img src="images/fotoalbum_mogelijkheden.jpg" alt="Fotoalbum functie" width="20" height="19" /> </td>
            <td width="96%">Opnieuw downloaden van foto's op volle resolutie</td>
          </tr>
          <tr>
            <td height="23" align="center" valign="baseline"><img src="images/fotoalbum_mogelijkheden.jpg" alt="Fotoalbum functie" width="20" height="19" /> </td>
            <td>Downloaden van foto op gewenste grootte. Onze server verkleint de foto's voor u, zodat u dat zelf niet meer moet doen in een programma</td>
          </tr>
          <tr>
            <td height="23" align="center" valign="baseline"><img src="images/fotoalbum_mogelijkheden.jpg" alt="Fotoalbum functie" width="20" height="19" /> </td>
            <td>Automatisch draaien van foto's naar juiste positie</td>
          </tr>

          <tr>
            <td height="23" align="center" valign="baseline"><img src="images/fotoalbum_mogelijkheden.jpg" alt="Fotoalbum functie" width="20" height="19" /> </td>
            <td>Mogelijk om handmatig een foto anders te draaien; mogelijkheid om grote hoeveelheid foto's tegelijk te draaien.</td>
          </tr>
          <tr>
            <td height="23" align="center" valign="baseline"><img src="images/fotoalbum_mogelijkheden.jpg" alt="Fotoalbum functie" width="20" height="19" /> </td>
            <td>Omzetten naar zwart/wit (grijswaarden); mogelijkheid om grote hoeveelheden foto's tegelijk om te zetten naar grijswaarden.</td>
          </tr>
          <tr>
            <td height="23" align="center" valign="baseline"><img src="images/fotoalbum_mogelijkheden.jpg" alt="Fotoalbum functie" width="20" height="19" /> </td>
            <td>EXIF gegevens blijven behouden. EXIF gegevens worden getoond bij foto. (EXIF gegevens = sluitertijd, datum foto genomen, diafragma,...)</td>
          </tr>
          <tr>
            <td height="23" align="center" valign="baseline"><img src="images/fotoalbum_mogelijkheden.jpg" alt="Fotoalbum functie" width="20" height="19" /> </td>
            <td>Elk album heeft een uniek adres, dat je zelf kan kiezen en eenvoudig te onthouden is</td>
          </tr>
          
            <td height="23" align="center" valign="baseline"><img src="images/fotoalbum_mogelijkheden.jpg" alt="Fotoalbum functie" width="20" height="19" /> </td>
            <td>Diashow album; waarbij deze ook kan worden gepauzeerd, snelheid kan worden ingesteld, naar een vorige/volgende foto kan worden gegaan,...</td>
          </tr>
          <tr>
            <td height="23" align="center" valign="baseline"><img src="images/fotoalbum_mogelijkheden.jpg" alt="Fotoalbum functie" width="20" height="19" /> </td>
            <td>Aanpassen album aan eigen wensen: kleuren aanpassen, achtergrond, lettertype, lettergrootte,...</td>
          </tr>
          <tr>
            <td height="23" align="center" valign="baseline"><img src="images/fotoalbum_mogelijkheden.jpg" alt="Fotoalbum functie" width="20" height="19" /> </td>
            <td>Mogelijk om vrije tekst (of HTML) toe te voegen boven- of onderaan fotoalbum</td>
          </tr>
          <tr>
            <td height="23" align="center" valign="baseline"><img src="images/fotoalbum_mogelijkheden.jpg" alt="Fotoalbum functie" width="20" height="19" /> </td>
            <td>Trefwoorden (keywords) kunnen ingegeven worden bij foto</td>
          </tr>
          <tr>
            <td height="23" align="center" valign="baseline"><img src="images/fotoalbum_mogelijkheden.jpg" alt="Fotoalbum functie" width="20" height="19" /> </td>
            <td>Er kan gezocht worden naar foto's</td>
          </tr>
          <tr>
            <td height="23" align="center" valign="baseline"><img src="images/fotoalbum_mogelijkheden.jpg" alt="Fotoalbum functie" width="20" height="19" /> </td>
            <td>Overzicht van foto's met zelfde trefwoord</td>
          </tr>
          <tr>
            <td height="23" align="center" valign="baseline"><img src="images/fotoalbum_mogelijkheden.jpg" alt="Fotoalbum functie" width="20" height="19" /> </td>
            <td>Eigen profielpagina waarop de gebruiker diverse eigen info kan op plaatsen</td>
          </tr>
          <tr>
            <td height="23" align="center" valign="baseline"><img src="images/fotoalbum_mogelijkheden.jpg" alt="Fotoalbum functie" width="20" height="19" /> </td>
            <td>Wissen van  foto's of foto-album is mogelijk zelf te doen</td>
          </tr>
          <tr>
            <td height="23" align="center" valign="baseline"><img src="images/fotoalbum_mogelijkheden.jpg" alt="Fotoalbum functie" width="20" height="19" /> </td>
            <td>Wel/geen kader rond foto</td>
          </tr>
          <tr>
            <td height="23" align="center" valign="baseline"><img src="images/fotoalbum_mogelijkheden.jpg" alt="Fotoalbum functie" width="20" height="19" /> </td>
            <td>Een tekstje op een foto zetten</td>
          </tr>
          <tr>
            <td height="23" align="center" valign="baseline"><img src="images/fotoalbum_mogelijkheden.jpg" alt="Fotoalbum functie" width="20" height="19" /> </td>
            <td>Standaard een tekstje zetten op alle foto's van een album</td>
          </tr>
          <tr>
            <td height="23" align="center" valign="baseline"><img src="images/fotoalbum_mogelijkheden.jpg" alt="Fotoalbum functie" width="20" height="19" /> </td>
            <td>Functie &quot;favoriete albums&quot; met zelfgekozen albums waarbij men deze eenvoudiger kan opvolgen</td>
          </tr>
          <tr>
            <td height="23" align="center" valign="baseline"><img src="images/fotoalbum_mogelijkheden.jpg" alt="Fotoalbum functie" width="20" height="19" /> </td>
            <td>E-card versturen van eigen foto; waarbij de e-card volledig aan eigen wensen kan aangepast worden (titel, achtergrondkleur, tekstkleur, muziekje, ...)</td>
          </tr>
          <tr>
            <td height="23" align="center" valign="baseline"><img src="images/fotoalbum_mogelijkheden.jpg" alt="Fotoalbum functie" width="20" height="19" /> </td>
            <td>Elke foto heeft een unieke url. Elk album heeft ook een unieke url, die u z&eacute;lf kunt kiezen.</td>
          </tr>
          <tr>
            <td height="23" align="center" valign="baseline"><img src="images/fotoalbum_mogelijkheden.jpg" alt="Fotoalbum functie" width="20" height="19" /> </td>
            <td>&quot;Mail een vriend&quot;-functie</td>
          </tr>
          <tr>
            <td height="23" align="center" valign="baseline"><img src="images/fotoalbum_mogelijkheden.jpg" alt="Fotoalbum functie" width="20" height="19" /> </td>
            <td>Gastenboek per album</td>
          </tr>
          <tr>
            <td height="23" align="center" valign="baseline"><img src="images/fotoalbum_mogelijkheden.jpg" alt="Fotoalbum functie" width="20" height="19" /> </td>
            <td>Bezoekers kunnen reacties plaatsen bij een foto</td>
          </tr>
          <tr>
            <td height="23" align="center" valign="baseline"><img src="images/fotoalbum_mogelijkheden.jpg" alt="Fotoalbum functie" width="20" height="19" /> </td>
            <td>Bezoekers kunnen een foto beoordelen (sterrensysteem: 1 tot 5 sterren)</td>
          </tr>
          <tr>
            <td height="23" align="center" valign="baseline"><img src="images/fotoalbum_mogelijkheden.jpg" alt="Fotoalbum functie" width="20" height="19" /> </td>
            <td>&quot;Mijn selectie&quot; waarbij foto's van eigen album of andere albums kunnen worden samengezet om verder te verwerken. Bvb. om in een nieuw album te zetten, om deze af te drukken, downloaden,...</td>
          </tr>
          <tr>
            <td height="23" align="center" valign="baseline"><img src="images/fotoalbum_mogelijkheden.jpg" alt="Fotoalbum functie" width="20" height="19" /> </td>
            <td>RSS voor elk fotoalbum en voor elke gebruiker</td>
          </tr>
          <tr>
            <td height="23" align="center" valign="baseline"><img src="images/fotoalbum_mogelijkheden.jpg" alt="Fotoalbum functie" width="20" height="19" /> </td>
            <td>Statistieken van gebruiker algemeen, per album en zelfs per foto.</td>
          </tr>

          <tr>
            <td height="23" align="center" valign="baseline"><img src="images/fotoalbum_mogelijkheden.jpg" alt="Fotoalbum functie" width="20" height="19" /> </td>
            <td>Mogelijkheid om specifieke gebruikers te bannen op &eacute;&eacute;n of meerdere (of standaard op alle)  eigen fotoalbums</td>
          </tr>
          <tr>
            <td height="23" align="center" valign="baseline"><img src="images/fotoalbum_mogelijkheden.jpg" alt="Fotoalbum functie" width="20" height="19" /> </td>
            <td>Volgorde aanpassen van foto's in een album</td>
          </tr>
          <tr>
            <td height="23" align="center" valign="baseline"><img src="images/fotoalbum_mogelijkheden.jpg" alt="Fotoalbum functie" width="20" height="19" /> </td>
            <td>Teksten plaatsen in album, zodat bvb. in een diashow ook teksten kunnen geplaatst worden</td>
          </tr>
          <tr>
            <td height="23" align="center" valign="baseline"><img src="images/fotoalbum_mogelijkheden.jpg" alt="Fotoalbum functie" width="20" height="19" /> </td>
            <td>Toevoegen van foto's die reeds op internet staan (via een url toevoegen)</td>
          </tr>
          <tr>
            <td height="23" align="center" valign="baseline"><img src="images/fotoalbum_mogelijkheden.jpg" alt="Fotoalbum functie" width="20" height="19" /> </td>
            <td>Zoekmachine voor foto's van alle gebruikers samen (enkel de foto's die publiek toegankelijk zijn)</td>
          </tr>
          <tr>
            <td height="23" align="center" valign="baseline"><img src="images/fotoalbum_mogelijkheden.jpg" alt="Fotoalbum functie" width="20" height="19" /> </td>
            <td>Album is geoptimaliseerd om zo goed mogelijk gevonden te worden door zoekmachines als Google</td>
          </tr>
          <tr>
            <td height="23" align="center" valign="baseline"><img src="images/fotoalbum_mogelijkheden.jpg" alt="Fotoalbum functie" width="20" height="19" /> </td>
            <td>Eenvoudig bladeren door fotoalbums door functie volgende/vorige foto</td>
          </tr>
          <tr>
            <td height="23" align="center" valign="baseline"><img src="images/fotoalbum_mogelijkheden.jpg" alt="Fotoalbum functie" width="20" height="19" /> </td>
            <td>Gebruiker kan zelf berichten verwijderen bij de reacties op foto's of uit gastenboek</td>
          </tr>
          <tr>
            <td height="23" align="center" valign="baseline"><img src="images/fotoalbum_mogelijkheden.jpg" alt="Fotoalbum functie" width="20" height="19" /> </td>
            <td>Foto's toevoegen via e-mail; zodat je zeer mobiel bent en overal foto's kan toevoegen. Mogelijkheid om via e-mail zo ook de titel en extra info bij de foto's te plaatsen. Mogelijkheid om extra e-mail adressen toe te voegen om bvb. vrienden of familie dezelfde mogelijkheid te geven. Mogelijk om deze functie open te stellen waardoor iedereen die het e-mail adres kent vrij foto's mag toevoegen.</td>
          </tr>
          <tr>
            <td height="23" align="center" valign="baseline"><img src="images/fotoalbum_mogelijkheden.jpg" alt="Fotoalbum functie" width="20" height="19" /> </td>
            <td>Functie om in &eacute;&eacute;n keer alles uit een gastenboek te wissen of alle reacties in &eacute;&eacute;n keer te wissen</td>
          </tr>
          <tr>
            <td height="23" align="center" valign="baseline"><img src="images/fotoalbum_mogelijkheden.jpg" alt="Fotoalbum functie" width="20" height="19" /> </td>
            <td>Mogelijkheid om albums open te zetten, de zogenaamde &quot;vrije albums&quot;. Hiermee kunnen ook de bezoekers foto's toevoegen aan je album. Mogelijk om te kiezen of deze rechtstreeks verschijnen, of dat je ze eerst nog wil goedkeuren. Mogelijk om deze functie enkel te geven aan vrienden of familie; of gewoon aan alle bezoekers.</td>
          </tr>
          <tr>
            <td height="23" align="center" valign="baseline"><img src="images/fotoalbum_mogelijkheden.jpg" alt="Fotoalbum functie" width="20" height="19" /> </td>
            <td>Inschakelen/uitschakelen van de functie dat bezoekers een reactie kunnen plaatsen, je een gastenboek hebt, bezoekers punten kunnen geven, bezoekers EXIF informatie kunnen zien, bezoekers je foto kunnen downloaden, je foto kunnen gebruiken in een e-card,...</td>
          </tr>
          <tr>
            <td height="23" align="center" valign="baseline"><img src="images/fotoalbum_mogelijkheden.jpg" alt="Fotoalbum functie" width="20" height="19" /> </td>
            <td>Printversie van fotopagina om gemakkelijk foto met extra informatie af te drukken</td>
          </tr>
          <tr>
            <td height="23" align="center" valign="baseline"><img src="images/fotoalbum_mogelijkheden.jpg" alt="Fotoalbum functie" width="20" height="19" /> </td>
            <td>Adres van album of gebruikersnaam is steeds te wijzigen.</td>
          </tr>
          <tr>
            <td height="23" align="center" valign="baseline"><img src="images/fotoalbum_mogelijkheden.jpg" alt="Fotoalbum functie" width="20" height="19" /> </td>
            <td>Overzicht van zelf verzonden e-cards</td>
          </tr>
          <tr>
            <td height="23" align="center" valign="baseline"><img src="images/fotoalbum_mogelijkheden.jpg" alt="Fotoalbum functie" width="20" height="19" /> </td>
            <td>Berichten versturen naar een gebruiker, en ook omgekeerd de mogelijkheid om berichten te ontvangen van andere gebruikers, zonder dat je e-mail adres doorgegeven wordt.</td>
          </tr>
          <tr>
            <td height="23" align="center" valign="baseline"><img src="images/fotoalbum_mogelijkheden.jpg" alt="Fotoalbum functie" width="20" height="19" /> </td>
            <td>Je eigen vrienden en familie organiseren. Mogelijkheid om per album specifiek bepaalde vrienden of familie juist wel, of juist niet toegang te geven waardoor 100% flexibiliteit mogelijk is.</td>
          </tr>
          <tr>
            <td height="23" align="center" valign="baseline"><img src="images/fotoalbum_mogelijkheden.jpg" alt="Fotoalbum functie" width="20" height="19" /> </td>
            <td>Eenvoudige teksteditor om met opmaak tekst bij een foto te plaatsen en zo eenvoudig HTML te genereren.</td>
          </tr>
          <tr>
            <td height="23" align="center" valign="baseline"><img src="images/fotoalbum_mogelijkheden.jpg" alt="Fotoalbum functie" width="20" height="19" /> </td>
            <td>Elke foto heeft een link (uniek internetadres) dat je kan gebruiken voor in een e-mail of voor op een website.</td>
          </tr>
          <tr>
            <td height="23" align="center" valign="baseline"><img src="images/fotoalbum_mogelijkheden.jpg" alt="Fotoalbum functie" width="20" height="19" /> </td>
            <td>Reservekopie maken en terug downloaden van volledig fotoalbum</td>
          </tr>
          <tr>
            <td height="23" align="center" valign="baseline"><img src="images/fotoalbum_mogelijkheden.jpg" alt="Fotoalbum functie" width="20" height="19" /> </td>
            <td>Onthouden van logingegevens zodat eenvoudig opnieuw ingelogd kan worden</td>
          </tr>
          <tr>
            <td height="23" align="center" valign="baseline"><img src="images/fotoalbum_mogelijkheden.jpg" alt="Fotoalbum functie" width="20" height="19" /> </td>
            <td>Speciaal fotografieforum waar mensen met elkaar kunnen communiceren over een gedeelde interesse en ook problemen kunnen voorleggen</td>
          </tr>
          <tr>
            <td height="23" align="center" valign="baseline"><img src="images/fotoalbum_mogelijkheden.jpg" alt="Fotoalbum functie" width="20" height="19" /> </td>
            <td>Ondersteuning voor de foto-albums. Gebruikers met problemen proberen we zo goed mogelijk te helpen; zonder kosten.</td>
          </tr>
          <tr>
            <td height="23" align="center" valign="baseline"><img src="images/fotoalbum_mogelijkheden.jpg" alt="Fotoalbum functie" width="20" height="19" /> </td>
            <td>SeniorenNet maakt maximaal reclame voor een fotoalbum indien het toegankelijk is voor iedereen; dit ondermeer in vermeldingen bij de categorie&euml;n, bij de toplijsten, recent toegevoegde foto's, zoekfunctie,...</td>
          </tr>
          <tr>
            <td height="23" align="center" valign="baseline"><img src="images/fotoalbum_mogelijkheden.jpg" alt="Fotoalbum functie" width="20" height="19" /> </td>
            <td>Je kan zelf de copyright tekst aanpassen bij foto's en zo bvb. aangeven dat een foto ook mag gebruikt worden voor niet-commerci&euml;le doeleinden, een foto vrij te gebruiken is, een foto te koop is, een foto juist niet mag gebruikt worden zonder toestemming,... (ook een vrije tekst op te geven)</td>
          </tr>
          <tr>
            <td height="23" align="center" valign="baseline"><img src="images/fotoalbum_mogelijkheden.jpg" alt="Fotoalbum functie" width="20" height="19" /> </td>
            <td>Inschakelen/uitschakelen van fotodetails en wel/niet zichtbaar maken van statistieken van aantal keren een foto werd gezien.</td>
          </tr>
          <tr>
            <td height="23" align="center" valign="baseline"><img src="images/fotoalbum_mogelijkheden.jpg" alt="Fotoalbum functie" width="20" height="19" /> </td>
            <td>Aanpassen META-tags van je fotoalbum (meta tag description, keywords en robots)</td>
          </tr>
          <tr>
            <td height="23" align="center" valign="baseline"><img src="images/fotoalbum_mogelijkheden.jpg" alt="Fotoalbum functie" width="20" height="19" /> </td>
            <td>Verwittigd worden indien bij een favoriet album nieuwe foto's werden toegevoegd</td>
          </tr>
          <tr>
            <td height="23" align="center" valign="baseline"><img src="images/fotoalbum_mogelijkheden.jpg" alt="Fotoalbum functie" width="20" height="19" /> </td>
            <td>Via e-mail verwittigd worden indien een gebruiker een bericht toevoegen in je gastenboek. Mogelijkheid om berichten in gastenboek enkel te laten verschijnen nadat je ze eerst goedgekeurd hebt.</td>
          </tr>
          <tr>
            <td height="23" align="center" valign="baseline"><img src="images/fotoalbum_mogelijkheden.jpg" alt="Fotoalbum functie" width="20" height="19" /> </td>
            <td>Vrienden/familie uitnodigen voor een afgesloten priv&eacute; album, waarna ze eenvoudig je album kunnen bekijken via eenvoudige registratieprocedure.</td>
          </tr>
          <tr>
            <td height="23" align="center" valign="baseline"><img src="images/fotoalbum_mogelijkheden.jpg" alt="Fotoalbum functie" width="20" height="19" /> </td>
            <td>Diashow werkt voor een onbeperkt aantal foto's. Geen limieten op het aantal foto's die in een diashow  kunnen!</td>
          </tr>
          <tr>
            <td height="23" align="center" valign="baseline"><img src="images/fotoalbum_mogelijkheden.jpg" alt="Fotoalbum functie" width="20" height="19" /> </td>
            <td>Diashow e-card versturen: je verstuurd &eacute;&eacute;n kaartje naar iemand, maar op de e-card komen meerdere foto's afwisselend zichtbaar. Voor de mensen die niet kunnen kiezen welke foto ze versturen!</td>
          </tr>
          <tr>
            <td height="23" align="center" valign="baseline"><img src="images/fotoalbum_mogelijkheden.jpg" alt="Fotoalbum functie" width="20" height="19" /> </td>
            <td>En jawel, nog m&eacute;&eacute;r! Bovendien blijft een team permanent werken om de albums nog te verbeteren, nog betrouwbaarder te maken en nog meer functies toe te voegen!</td>
          </tr>
        </table>
        <p class="MsoNormal"><br />
        </p>
      <p align="left">&nbsp;</p></td>
    </tr>
  </table>
</div>
<br></td>

    </div> <!-- /content-->
    <div id="widgets" class="columns">
<div id="fb-root"></div>
<script>(function (d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id))
            return;
        js = d.createElement(s);
        js.id = id;
        js.src = "//connect.facebook.net/nl_BE/sdk.js#xfbml=1&version=v2.6&appId=513131492093790";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>

<div class="fb-page mt1 mb1" data-href="https://www.facebook.com/seniorennet" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><div class="fb-xfbml-parse-ignore"><blockquote cite="https://www.facebook.com/seniorennet"><a href="https://www.facebook.com/seniorennet">Seniorennet</a></blockquote></div></div>


      <div id="printpage" class="widget-item">
        <div id="printpage-inner" class="padding-inner">
          <table width="100%">
            <tr>
              <td valign="middle"><a href="#" onClick="
              var str = document.URL;
              var url_check = str.indexOf( '?' );
              if ( url_check != -1 ) {
                 var urlke = window.location + '&afdrukken=ja';
              }
              else
              {
                var urlke = window.location + '?afdrukken=ja';
              }
              newwindow=window.open(urlke,'Afdrukken','height=800,width=700,menubar=no,scrollbars=yes,resizable=yes,status=no,toolbar=no,menubar=no');
              if (window.focus) {newwindow.focus()} return false;">Print pagina</a></td>
              <td valign="middle" align="right" width="20%"><img src="//www.seniorennet.be/images/icons/print.gif" alt="Pagina afdrukken" /></td>
            </tr>
          </table>
        </div>
      </div>
      <div class="widget-item">
        <div class="right-promobox">
          <div id='div-gpt-ad-1462356799442-3'>
            <script type='text/javascript'>
            googletag.cmd.push(function() { googletag.display('div-gpt-ad-1462356799442-3'); });
            </script>
          </div>
          <div id='div-gpt-ad-1462356799442-4'>
            <script type='text/javascript'>
            googletag.cmd.push(function() { googletag.display('div-gpt-ad-1462356799442-4'); });
            </script>
          </div>
          <div style="margin: 1em 0;" id='div-gpt-ad-1462356799442-1'>
            <script type='text/javascript'>
            googletag.cmd.push(function() { googletag.display('div-gpt-ad-1462356799442-1'); });
            </script>
          </div>
          <div id='div-gpt-ad-1462356799442-2'>
            <script type='text/javascript'>
            googletag.cmd.push(function() { googletag.display('div-gpt-ad-1462356799442-2'); });
            </script>
          </div>
        </div>
      </div>

      
      
      <div id="poll-wrap">
          
      </div>
      <script type="text/javascript">$('#poll-wrap').load('//www.seniorennet.be/polls/ajax-view');</script>

    </div>

<div id="pagefooter" class="columns">
  <div id="bookmarks">
    <div id="bookmarks-inner">
      <img src="https://www.facebook.com/favicon.ico" alt="facebook" />
      <a href="#" onclick="window.open('https://www.facebook.com/sharer/sharer.php?u='+encodeURIComponent(location.href),'facebook-share-dialog','width=626,height=436');return false;">
        Deel op Facebook
      </a>
    </div>
  </div>

  <a href="https://www.seniorennet.be/pages/redactie#abonneren">
    <div class="nb-inschrijven-footer">
      <form name="input" action="https://www.seniorennet.be/pages/redactie#abonneren" method="post">
      	<input name="state" type="hidden" id="state" value="I">
      	<input name="fv" type="hidden" id="hdfv" value="1">
    	<input name="ruri" type="hidden" id="hdruri" value="/help_fotoalbum_mogelijkheden_foto_fotos.php">
        <input type="email" name="emailadres" class="input" placeholder="emailadres" onClick="window.open('https://www.seniorennet.be/pages/redactie#abonneren');">
        <input type="submit" value="" class="button">
      </form>
    </div>
  </a>


  <div id="support">
  	<div id="support-inner">
    	Een vraag of een probleem op SeniorenNet? <br />
  		Kijk dan in het <a href="http://www.seniorennet.be/Pages/Overige/support.php">Helpcentrum (klik hier)</a>.
      Als je het daar niet kan vinden, helpen we je via e-mail op <a href="mailto:support@seniorennet.be">support@seniorennet.be</a>.
  	</div>
  </div>

</div>
</div>     
    

  <div id="footer_bottom"> Copyright &copy; 2001-2021 SeniorenNet - Alle rechten voorbehouden - 
  <a href="https://www.seniorennet.be/Pages/Overige/advert.php">Adverteren</a> | 
  <a href="https://www.seniorennet.be/Pages/Overige/support.php"  rel="nofollow">Contacteer ons</a> | 
  <a href="https://www.seniorennet.be/page/info/logo">Logo SeniorenNet</a> | 
  <a href="https://www.seniorennet.be/Pages/Overige/partners.php">Partners</a> | 
  <a href="https://www.seniorennet.be/Pages/Overige/over_ons.php">Over ons</a> | 
  <a href="https://www.seniorennet.be/Pages/Overige/privacy.php"  rel="nofollow">Privacy</a> | 
  <a href="https://www.seniorennet.be/page/info/disclaimer"  rel="nofollow">Disclaimer</a>

    </div>
  <div class="clear"></div>
</div>

<div class="clear"></div>


<script src="https://www.seniorennet.be/cache_js/foundation.js?v2018"></script>
<script type="text/javascript">
$(function(){
  $(document).foundation();
  $(window).scroll(function() {
    if ($(this).scrollTop() > 56){  
      $('#main-menubar').addClass("is-stuck");
      $('#main-menubar').removeClass("is-at-top");
      $("#main-menubar").css({
        'width': ($("#user-menubar").width() + 'px')
      });
      $("#user-menubar").css({
        'margin-bottom': ($("#main-menubar").height() + 'px')
      });
    } else{
      $('#main-menubar').addClass("is-at-top");
      $('#main-menubar').removeClass("is-stuck");
      $("#user-menubar").css({
        'margin-bottom': '0px'
      });
    }
  });
});
</script>

<!--[if lte IE 9]>
<script>
    var $buoop = {c:2,l:'nl'};
    function $buo_f(){
     var e = document.createElement("script");
     e.src = "//browser-update.org/update.min.js";
     document.body.appendChild(e);
    };
    try {document.addEventListener("DOMContentLoaded", $buo_f,false)}
    catch(e){window.attachEvent("onload", $buo_f)}


    $('.has-submenu').hover(function () {
        var left = $(this).offset().left - 245;
        $(this).find('.submenu').addClass('vertical').addClass('js-dropdown-active').css('left', left);
    }, function () {
        $(this).find('.submenu').removeClass('js-dropdown-active');
    });
</script>
<![endif]-->


<!--[if lt IE 9]>
    <script src="/js/ie8.js"></script>
<![endif]-->
</body>
</html>
</body>
</html>
