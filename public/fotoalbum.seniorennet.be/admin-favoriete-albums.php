<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Seniorennet Fotoalbums - Aanmelden</title>
</head>

<body link="#000000" vlink="#000000" alink="#000000" topmargin="0" rightmargin="1" leftmargin="0">
  <table width="100%" border="0" cellspacing="0">
  <tr>
          <td width="20%" rowspan="4" bgcolor="#DCE2F4"><a href="http://fotoalbum.seniorennet.be"><img src="https://images.seniorennet.be/layout/logo-seniorennet.svg" height="50" hspace="2" vspace="2" border="0" /></a></td>
    <td height="4" colspan="3" bgcolor="#DCE2F4"></td>
  </tr>
  <tr>
    <td colspan="2" background="images/achtergrond3.gif"><font size="6">Inloggen / Registreren </font></td>
    <td width="219" background="images/achtergrond3.gif"><div align="right"><font size="2"><a href="index-albums.php"></a></font></div></td>
  </tr>
  <tr>
    <td height="1" colspan="2" bgcolor="#CCCCCC"></td>
    <td bgcolor="#CCCCCC" height="1"></td>
  </tr>
  <tr>
    <td height="1" bgcolor="#DCE2F4">&nbsp;</td>
    <td width="200" bgcolor="#DCE2F4">&nbsp;</td>
    <td bgcolor="#DCE2F4">&nbsp;</td>
  </tr>
  <tr>
    <td height="2" colspan="4" bgcolor="#DCE2F4"></td>
  </tr>
</table>
<table width="100%" border="0">
  <tr>
    <td width="100%" valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="0">
      <tr>
        <td width="100%"><p>U dient in te loggen of te registreren om van deze functies gebruik te kunnen maken.<br />
          Registreren is GRATIS en eenvoudig.
</p>
          <table width="100%" border="0" cellspacing="0" bordercolor="#D4DAEC">
          <tr>
            <td bgcolor="#D4DAEC"><strong><font size="2">Inloggen</font></strong></td>
          </tr>
          <tr>
            <td><table width="100%" border="0" cellpadding="0" cellspacing="0">

              <tr>
                <td width="100%" valign="top">
                    <form action="inloggen.php" method="post">     
                    <input name="login" value="login" type="hidden" size="15" />
                    <table width="100%" border="0">
                      <tr>
                        <td>Gebruikersnaam:</td>
                        <td><input name="username" type="text" size="15" value=""/></td>
                                               </tr>
                      <tr>
                        <td>Paswoord:</td>
                        <td><input name="password" type="password" value="" size="15" /></td>
                      </tr>
                      <tr>
                      <td></td><td>
                                                                  </td>
                      </tr>
                      <tr>
                        <td>Bewaar gegevens</td>
                        <td><input type="checkbox" name="savesettings" value="1" /></td>
                      </tr>
                      <tr>
                        <td>&nbsp;</td>
                        <td><label></label><input type="image" src="/images/buttons/verzenden.gif" width="74" height="16" border="0" /><br />
                        </form>
                        <a href="http://www.seniorennet.be/Pages/Overige/paswoord_vergeten.php"><em>Paswoord vergeten? </em></a></td>
                      </tr>
                    </table>
                
                
                   </td>
              </tr>
            </table></td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td height="5"></td>
      </tr>
      <tr>
        <td>
          <table width="100%" border="0" cellspacing="0" bordercolor="#D4DAEC">
            <tr>
              <td bgcolor="#D4DAEC"><font size="2"><strong>Registreren</strong></font></td>
            </tr>
            <tr>
              <td><p>Waarom registreren?</p>
                <ul>
                  <li>Gratis, snel en eenvoudig</li>
                  <li>U kan reacties plaatsen, foto's beoordelen, meedoen aan fotowedstrijden, gastenboeken tekenen, foto's downloaden,... </li>
                  <li>U heeft hiermee 101 extra functies</li>
                  <li>U kan uw eigen foto-albums aanmaken!</li>
                  </ul>
                <table width="80%" border="1" align="center" cellpadding="0" cellspacing="0" bordercolor="#006666">
                  <tr>
                    <td height="35" bgcolor="#E4E4C9"><div align="center"><a href="registreren.php"><strong>Registreer, klik hier </strong></a></div></td>
                  </tr>
                </table>                </td>
            </tr>
          </table>          </td>
      </tr>
      <tr>
        <td height="5"></td>
      </tr>
    </table>
      
    </td>
    <td width="300" valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="0">
                     <tr>
                <td colspan="2" valign="top" bgcolor="#FFCC33"><a href="http://fotoalbum.seniorennet.be/registreren.php">Uw eigen gratis fotoalbum? Klik hier!</a></td>
                </tr>      <tr>
        <td height="5"></td>
      </tr>
            <tr>
        <td>
        <p align="center">
			        </p>
        </td>
      </tr>
      <tr>
        <td height="5"></td>
      </tr>
    </table></td>
  </tr>
</table>
<br />
<table width="100%" border="0">

  <tr>

    <td height="2" bgcolor="#D4DAEC"></td>

  </tr>

  <tr>

    <td><div align="center"><a href="http://fotoalbum.seniorennet.be"><font size="2">Gratis fotoalbum, beginpagina </font></a><font size="2">

    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|

    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="http://fotoalbum.seniorennet.be/registreren.php">Start uw eigen gratis fotoalbum, klik hier</a>

    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

    <a href="http://www.seniorennet.be"> SeniorenNet startpagina </a>

    &nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

    <a href="http://fotoalbum.seniorennet.be/help.php">Help</a></font><br />

    <a href="http://fotoalbum.seniorennet.be/abuse.php?url=http://fotoalbum.seniorennet.be/inloggen.php"><font size="2">Fotoalbum tegen de regels? Meld het ons!</font></a><font size="2"> &nbsp;&nbsp;&nbsp;SeniorenNet heeft GEEN banden met de auteurs. </font></div></td>

  </tr>

</table>
<script type="text/javascript">
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-1117128-1']);
  _gaq.push(['_setDomainName', 'seniorennet.be']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();
</script></body>
</html>
