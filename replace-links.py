#!/usr/bin/python3

import re
import sys
import glob

cdn_url="https://fotos-fons.ams3.digitaloceanspaces.com/fotoalbum.seniorennet.be/fons/fotos/img"
pages_url="https://in-memoriam-fons.esperanzaproxima.net/fotoalbum.seniorennet.be/"

# article links and attachments
link_attachment = {}
postname_imgs = {}
# fotos for an article
fotos = {}

# load config values

def replace_urls(text, replace_me=None):

    # if specific replace_me urls are given	
	if replace_me: return text.replace(replace_me[0],replace_me[1])

    # else: use img_url_replace & link_url_replace vars
	for replace_url in img_url_replace:	  
	  text=text.replace(replace_url[0],replace_url[1])
	for replace_url in link_url_replace:	  
	  text=text.replace(replace_url[0],replace_url[1])    	      	  
	return text

count=0
for filename in sys.argv:
  print(filename)
  with open(filename, encoding='iso-8859-1') as filein :
    filedata = filein.read()           
            
    #correct foto links 
    re_match_url="https?:\/\/fotoalbum\.seniorennet\.be\/incl\/getimage2\.php\?imageid=([0-9]+)&a?m?p?;?albumid=[0-9]+&a?m?p?;?typeid=[0-9]"    
    foundmatch = re.search(re_match_url,filedata)    
    while foundmatch: 
      filedata=replace_urls(filedata,(foundmatch.group(),cdn_url+foundmatch.group(1)+'.jpg'))
      print(foundmatch.group(),'=>',cdn_url+foundmatch.group(1)+'.jpg?')            
      foundmatch = re.search(re_match_url,filedata)            		
      count+=1

    #correct fotoalbum links
    re_match_url="(http:\/\/fotoalbum\.seniorennet\.be\/)"    
    foundmatch = re.search(re_match_url,filedata)    
    while foundmatch: 
      filedata=replace_urls(filedata,(foundmatch.group(),pages_url))
      print(foundmatch.group(),'=>',pages_url)            
      foundmatch = re.search(re_match_url,filedata)            		
      count+=1
      
    with open(filename, 'w') as fileout: fileout.write(filedata)
print('made: ',count,'replacements in: ',filename)    
      
